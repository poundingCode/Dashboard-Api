﻿using System.Collections.Generic;

namespace RavenVision.Model
{
    public class SlaSummary
    {
        public string SLA_NUMBER { get; set; }
        
        public string SERVICE_LEVEL { get; set; }

        public string SERVICE_CATEGORY { get; set; }
        
        public string ACCEPTED_QUALITY_LEVEL { get; set; }
        
        public Dictionary<string, object> Score { get; set; }
    }
}
