﻿namespace RavenVision.Model
{
    public  class TestResult
    {
        public string EndPoint { get; set; }

        public int? StatusCode { get; set; }

        public string Duration {  get; set; }
    }
}
