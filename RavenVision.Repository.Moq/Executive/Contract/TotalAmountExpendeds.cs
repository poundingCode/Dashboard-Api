﻿using RavenVision.Model.Executive.Contract;
using System;
using System.Collections.Generic;

namespace RavenVision.Repository.Moq.Executive.Contract
{
    public static class TotalAmountExpendeds
    {
        public static IEnumerable<TotalAmountExpended> GetAll()
        {
            return new List<TotalAmountExpended> {
                new TotalAmountExpended { Id = Guid.NewGuid(), Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit", ModifiedDate = DateTime.Now.AddDays(-3)},
                new TotalAmountExpended { Id = Guid.NewGuid(), Description = "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ", ModifiedDate = DateTime.Now.AddDays(-13) },
                new TotalAmountExpended { Id = Guid.NewGuid(), Description = "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ", ModifiedDate = DateTime.Now.AddDays(-7) },
                new TotalAmountExpended { Id = Guid.NewGuid(), Description = "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ", ModifiedDate = DateTime.Now.AddDays(-11) }
            };
        }
    }
}
