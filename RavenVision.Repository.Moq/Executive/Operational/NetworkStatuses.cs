﻿using RavenVision.Model.Executive.Operational;
using System;
using System.Collections.Generic;

namespace RavenVision.Repository.Moq
{
    public static class NetworkStatuses
    {
        public static IEnumerable<NetworkStatus> GetAll()
        {
            return new List<NetworkStatus> {
                new NetworkStatus { Id = Guid.NewGuid(), Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit", ModifiedDate = DateTime.Now.AddDays(-3)},
                new NetworkStatus { Id = Guid.NewGuid(), Description = "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ", ModifiedDate = DateTime.Now.AddDays(-13) },
                new NetworkStatus { Id = Guid.NewGuid(), Description = "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ", ModifiedDate = DateTime.Now.AddDays(-7) },
                new NetworkStatus { Id = Guid.NewGuid(), Description = "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ", ModifiedDate = DateTime.Now.AddDays(-11) }
            };
        }
    }
}
