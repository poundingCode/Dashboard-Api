﻿using RavenVision.Model.Executive.Operational;
using System;
using System.Collections.Generic;

namespace RavenVision.Repository.Moq
{
    public static class OperationalStatuses
    {
        public static IEnumerable<OperationalStatus> GetAll()
        {
            return new List<OperationalStatus> {
                new OperationalStatus { Id = Guid.NewGuid(), Category = "Public", Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit", ModifiedDate = DateTime.Now.AddDays(-3)},
                new OperationalStatus { Id = Guid.NewGuid(), Category = "Government",  Description = "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ", ModifiedDate = DateTime.Now.AddDays(-13) },
                new OperationalStatus { Id = Guid.NewGuid(), Category = "Military",  Description = "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ", ModifiedDate = DateTime.Now.AddDays(-7) },
                new OperationalStatus { Id = Guid.NewGuid(), Category = "NGO",  Description = "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ", ModifiedDate = DateTime.Now.AddDays(-11) }
            };
        }
    }
}
