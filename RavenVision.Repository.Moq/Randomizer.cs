﻿using System;

namespace RavenVision.Repository.Moq
{
    public static class Randomiser
    {
        public static string RandomText(int length)
        {
            Random rand = new Random();

            // Choosing the size of string
            // Using Next() string 
            int randValue;
            string text = "";
            char letter;
            for (int i = 0; i < length; i++)
            {

                // Generating a random number.
                randValue = rand.Next(0, 26);

                // Generating random character by converting
                // the random number into character.
                letter = Convert.ToChar(randValue + 65);

                // Appending the letter to string.
                text += letter.ToString().ToUpper();
            }
            return text;
        }

        public static int RandomNumber(int min, int max)
        {
            Random rand = new Random();

            int randValue = rand.Next(min, max);

            return randValue;
        }
    }
}
