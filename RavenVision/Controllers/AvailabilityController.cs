﻿
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Identity.Web;
using RavenVision.DataSources.API;
using RavenVision.Model;
using RavenVision.Services;

namespace RavenVision.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AvailabilityController : BaseController<AvailabilityController>
    {
        static readonly string[] scopeRequiredByApi = new string[] { "access_as_user" };
        private readonly CatalogController catalogController = null;
        private readonly NetworkController networkController = null;
        private readonly PurchasingController purchasingController = null;
        private readonly ServiceController serviceController = null;
        private readonly SecurityController securityController = null;
        private readonly SnowController snowController = null;
        public AvailabilityController(ITokenAcquisition tokenAcquisition, IConfiguration iconfig, IOboIdentityApi oboIdentityApi, ILogger<AvailabilityController> logger, ILogger<CatalogController> catalogLogger, ILogger<NetworkController> networkLogger, ILogger<PurchasingController> purchasingLogger, ILogger<SecurityController> securityLogger, ILogger<ServiceController> serviceLogger, ILogger<SnowController> snowLogger)  : base(iconfig, "", logger)
        {
            catalogController = new CatalogController(iconfig, catalogLogger);
            networkController = new NetworkController(iconfig, networkLogger);
            purchasingController = new PurchasingController(iconfig, purchasingLogger);
            serviceController = new ServiceController(iconfig, serviceLogger);
            snowController = new SnowController(tokenAcquisition, snowLogger, oboIdentityApi, iconfig);
            securityController = new SecurityController(iconfig, securityLogger);
        }

        [HttpGet("errorsOnly")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]

        public async Task<ActionResult<ICollection<TestResult>>> GetDataLakeTest(bool errorsOnly=true)
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            return Ok(await TestApi(errorsOnly));
        }

        private async Task<ICollection<TestResult>> TestApi(bool errorsOnly)
        {
            var results = new List<TestResult>();
            results.AddRange(await TestCatalog(errorsOnly));
            results.AddRange(await TestNetwork(errorsOnly));
            results.AddRange(await TestPurchasing(errorsOnly));
            results.AddRange(await TestService(errorsOnly));
            results.AddRange(await TestSecurity(errorsOnly));
           // results.AddRange(await TestSnow(errorsOnly));
            return results;
        }


        private static TestResult TestError(TestResult result, bool errorsOnly)
        {
            var status = result.StatusCode.GetValueOrDefault(600);
            if (errorsOnly)
            {
                if (status >= 300)
                { 
                    return result; 
                }
                return null;
            }
            return result;
        }

        private async Task<List<TestResult>> TestCatalog(bool errorsOnly)
        {
            var results = new List<TestResult>();

            var result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await catalogController.GetCotsByTaskOrder(), "catalogController.CotsByTaskOrder"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await catalogController.GetCotsPurchaseDetails("2"), "catalogController.CotsPurchaseDetails"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await catalogController.GetCotsPurchasesByMonth(), "catalogController.CotsPurchasesByMonth"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await catalogController.GetCotsPurchaseDetailsAll(), "catalogController.CotsPurchaseDetailsAll"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await catalogController.GetCotsSummary(), "catalogController.GetCotsSummary"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await catalogController.GetCSV(), "catalogController.CSV"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await catalogController.GetServicesByTaskOrder(), "catalogController.ServicesByTaskOrder"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await catalogController.GetServicesPurchasesByMonth(), "catalogController.PurchasesByContract"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await catalogController.GetServicePurchaseDetails("2"), "catalogController.ServicePurchaseDetails"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await catalogController.GetServicePurchaseDetailsAll(), "catalogController.ServicePurchaseDetailsAll"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await catalogController.GetServiceSummary(), "catalogController.GetServiceSummary"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await catalogController.GetXLM(), "catalogController.PurchasesByContract"), errorsOnly);
                TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await catalogController.GetPurchasesByContract(), "catalogController.PurchasesByContract"), errorsOnly);
            
            return results;
        }

        private async Task<List<TestResult>> TestNetwork(bool errorsOnly)
        {
            var results = new List<TestResult>();
            var result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetAvailabilityCount(), "networkController.AvailabilityCount"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetAvailabilityHistory(2), "networkController.AvailabilityHistory(2)"), errorsOnly); 
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetCapAndUtilRollup(), "networkController.GetCapAndUtilRollup"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetCapAndUtilRollupPhysical(), "networkController.GetCapAndUtilRollupPhysical"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetCapAndUtilRollupVirtual(), "networkController.GetCapAndUtilRollupVirtual"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetCapacityAndUtilizationLatest(), "networkController.GetCapacityAndUtilizationLatest"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetDeviceHealth(), "networkController.DeviceHealth"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetDeviceHealth(2), "networkController.DeviceHealth(2) -- commented out"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetDeviceHealthByMonth(), "networkController.GetDeviceHealthByMonth"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetDeviceHealthThresholdCPU(), "networkController.GetDeviceHealthThresholdCPU"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetDeviceHealthThresholdMem(), "networkController.GetDeviceHealthThresholdMem"), errorsOnly);
            if (result != null)
                results.Add(result);


            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetDeviceList(), "networkController.DeviceList"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetDeviceHealthChart(), "networkController.GetDeviceHealthChart"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetDeviceMetrics(2), "networkController.DeviceMetrics(2)"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetDeviceThresholds(2), "networkController.DeviceThresholds(2)"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetInterfaceList(), "networkController.InterfaceList"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetInterfaceMetrics(2), "networkController.InterfaceMetrics(2)"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetLatencyAverage(), "networkController.LatencyAverage"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetNetworkMetrics(), "networkController.NetworkMetrics"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetSiLoPowerPackList(), "networkController.SiLoPowerPackList"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetTaskOrderList(), "networkController.TaskOrderList"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.GetUnavailableDeviceList(), "networkController.UnavailableDeviceList"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await networkController.PostNetworkMetrics(), "networkController.NetworkMetrics(Post)"), errorsOnly);
            if (result != null)
                results.Add(result);

            return results;
        }

        private async Task<List<TestResult>> TestPurchasing(bool errorsOnly)
        {
            var results = new List<TestResult>();

            var result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetComponentTaskOrderTotal(), "purchasingController.ComponentTaskOrderTotal"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetComponentTotal(), "purchasingController.ComponentTotal"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetContractLevelSummaryCosts(), "purchasingController.ContractLevelSummaryCosts"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetContractLevelSummaryCostsExportCSV(), "purchasingController.GetContractLevelSummaryCostsExportCSV"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetContractList(), "purchasingController.ContractList"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetContractSpendingVsCeiling(), "purchasingController.ContractSpendingVsCeiling"), errorsOnly);
            if (result != null)
                results.Add(result);
            
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetExportBillingData_CSV(), "purchasingController.ExportBillingData_CSV"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetExportBillingData_XML(), "purchasingController.ExportBillingData_XML"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetExportBilling_Filter_XML(), "purchasingController.GetExportBilling_Filter_XML"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetExportilling_Filter_CSV(), "purchasingController.GetExportilling_Filter_CSV"), errorsOnly);
            if (result != null)
                results.Add(result);


            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetExportFundingData_TaskOrderClinCategory_CSV(), "purchasingController.ExportFundingData_TaskOrderClinCategory_CSV"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetExportFundingData_TaskOrderClinCategory_XML(), "purchasingController.ExportFundingData_TaskOrderClinCategory_XML"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetIdiqClinTotalByCategory(), "purchasingController.IdiqClinTotalByCategory"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetLaborHoursOverallForYear(2022), "purchasingController.GetLaborHoursOverallForYear"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetLaborHourSpendingByMonthChart(), "purchasingController.GetLaborHourSpendingByMonthChart"), errorsOnly);
            if (result != null)
                results.Add(result);


            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetLaborHourExportCSV(), "purchasingController.GetLaborHourExportCSV"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetLaborHourExportXML(), "purchasingController.GetLaborHourExportXML"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetLaborHourSpending_Everything(), "purchasingController.GetLaborHourSpending_Everything"), errorsOnly);
            if (result != null)
                results.Add(result);
            
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetLaborHourSpending_Overall(), "purchasingController.LaborHourSpending_Overall"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetLaborHourSpending_Quarter(2, 2022), "purchasingController.LaborHourSpending_Quarter"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetLaborHourSpending_Year(2), "purchasingController.LaborHourSpending_Year"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetPercentagesAwardedVsContractCeiling(), "purchasingController.GetPercentagesAwardedVsContractCeiling"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetPercentagesFundedVsContractCeiling(), "purchasingController.GetPercentagesFundedVsContractCeiling"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetPercentagesSpentVsContractCeiling(), "purchasingController.GetPercentagesSpentVsContractCeiling"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetResourcesMonthlyCSP(), "purchasingController.GetResourcesMonthlyCSP"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetResourcesMonthlyCSPChart(), "purchasingController.GetResourcesMonthlyCSPChart"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetResourcesMonthlyHCE(), "purchasingController.GetResourcesMonthlyHCE"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetResourcesMonthlyHceChart(), "purchasingController.GetResourcesMonthlyHceChart"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetSpaceBasedExportCSV(), "purchasingController.GetSpaceBasedExportCSV"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetSpaceBasedExportXML(), "purchasingController.GetSpaceBasedExportXML"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetSpaceBasedOverall(), "purchasingController.GetSpaceBasedOverall"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetTaskOrderClinListData(2), "purchasingController.TaskOrderClinListData"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetTaskOrderList(2), "purchasingController.TaskOrderList"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetTaskOrderPurchasing(), "purchasingController.TaskOrderPurchasing"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetTaskOrderPurchasingExportCSV(), "purchasingController.GetTaskOrderPurchasingExportCSV"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetTaskOrderPurchasingByContractClin(2), "purchasingController.TaskOrderPurchasingByContractClin"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetTaskOrderPurchasingByIdiqClin(2), "purchasingController.TaskOrderPurchasingByIdiqClin"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetTaskOrderSpendingVsCeiling(), "purchasingController.TaskOrderSpendingVsCeiling"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetTaskOrderSpendingVsCeilingChart(), "purchasingController.GetTaskOrderSpendingVsCeilingChart"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetTotalContractCAGR(), "purchasingController.TotalContractCAGR"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetTotalContractExpenditureData(), "purchasingController.TotalContractExpenditureData"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetTotalPurchasingAll(), "purchasingController.TotalPurchasingAll"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetTotalPurchasingByMonthChart(), "purchasingController.GetTotalPurchasingByMonthChart"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetTotalPurchasingByMonthCotsChart(), "purchasingController.GetTotalPurchasingByMonthCotsChart"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetTotalPurchasingByMonthServicesChart(), "purchasingController.GetTotalPurchasingByMonthServicesChart"), errorsOnly);
            if (result != null)
                results.Add(result);



            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetTotalPurchasingByTaskOrderAndServiceCategory(), "purchasingController.GetTotalPurchasingByTaskOrderAndServiceCategory"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetTotalPurchasingByIdiqClin(), "purchasingController.TotalPurchasingByIdiqClin"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetTotalPurchasingByIdiqClinCategory(), "purchasingController.GetTotalPurchasingByIdiqClinCategory"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetTotalPurchasingByTaskOrder(), "purchasingController.TotalPurchasingByTaskOrder"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await purchasingController.GetTotalSpendingForYear(2022), "purchasingController.TotalSpendingForYear"), errorsOnly);
            if (result != null)
                results.Add(result);
           
            return results;
        }

        private async Task<List<TestResult>> TestService(bool errorsOnly)
        {
            var results = new List<TestResult>();

            var result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await serviceController.GetSLAFailed(), "serviceController.SLAFailed"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await serviceController.GetSlascore(), "serviceController.Slascore"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await serviceController.GetSlaSummary(), "serviceController.SlaSummary"), errorsOnly);
            if (result != null)
                results.Add(result);

            return results;
        }

        private async Task<List<TestResult>> TestSecurity(bool errorsOnly)
        {
            var results = new List<TestResult>();

            var result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await securityController.GetIncidentAll(), "securityController.GetIncidentAll"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await securityController.GetIncidentTicketSummary(), "securityController.GetIncidentTicketSummary"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await securityController.GetExportIncidentCSV(), "securityController.GetExportIncidentCSV"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await securityController.GetExportIncidentXML(), "securityController.GetExportIncidentXML"), errorsOnly);
            if (result != null)
                results.Add(result);

            return results;
        }

        private async Task<List<TestResult>> TestSnow(bool errorsOnly)
        {
            var results = new List<TestResult>();

            var result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await snowController.GetDayAgingByHardwareAsset(0,10), "snowController.GetDayAgingByHardwareAsset"), errorsOnly);
            if (result != null)
                results.Add(result);

            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await snowController.GetHardwareAssets(0, 10), "snowController.GetHardwareAssets"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await snowController.GetHardwareInventoryManagement(0, 10), "snowController.GetHardwareInventoryManagement"), errorsOnly);
            if (result != null)
                results.Add(result);
            result = TestError(await GetTestResponse<Task<ActionResult<string>>>(async () => await snowController.GetMaintenanceAgreementEndDates(0, 10), "snowController.GetMaintenanceAgreementEndDates"), errorsOnly);
            if (result != null)
                results.Add(result);

            return results;
        }
    }
}
