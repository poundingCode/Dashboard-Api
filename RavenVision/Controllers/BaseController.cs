﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RavenVision.DataSources.API;
using RavenVision.Exceptions;
using RavenVision.Model;

namespace RavenVision.Controllers
{
    public class BaseController<T> : ControllerBase
    {
        internal readonly ILogger<T> _logger;
        internal readonly ApiBuilder dataSource;

        public BaseController(IConfiguration iconfig, string area, ILogger<T> logger)
        {
            _logger = logger;
            this.dataSource = new ApiBuilder(iconfig, area);
        }

        internal async Task<ActionResult<string>> GetHttpResponse<ActionResult>(Func<Task<string>> func)
        {
            try
            {
                var data = await func();

                if (data == null)
                {
                    return NotFound();
                }

                return Ok(data);
            }
            catch (ApiException a)
            {
                _logger.LogError($"BaseController.GetHttpResponse ApiException: {a}");
                return Problem(a.Message, a.InnerException?.Message, a.StatusCode);
            }
            catch (Exception e)
            {
                _logger.LogError($"BaseController.GetHttpResponse ApiException: {e}");
                return Problem(e.Message, e.InnerException?.Message, 500);
            }
        }

        internal async Task<ActionResult<string>> GetJsonResponse<ActionResult>(Func<Task<ICollection<Dictionary<string, object>>>> func)
        {
            try
            {
                var data = await func();

                if (data == null)
                {
                    return NotFound();
                }
                if (data.Count == 0)
                {
                    return NoContent();
                }
                
                var json = JsonConvert.SerializeObject(data, Formatting.Indented);

                return Ok(json);
            }
            catch (ApiException a)
            {
                _logger.LogError($"BaseController.GetJsonResponse ApiException: {a}");
                return Problem(a.Message, a.InnerException?.Message, a.StatusCode);
            }
            catch (Exception e)
            {
                _logger.LogError($"BaseController.GetJsonResponse Exception: {e}");
                return Problem(e.Message, e.InnerException?.Message, 500);
            }
        }

        internal async Task<ActionResult<string>> GetJsonResponse<ActionResult>(Func<Task<Dictionary<string, object>>> func)
        {
            try
            {
                var data = await func();

                if (data == null)
                {
                    return NotFound();
                }
                if (data.Count == 0)
                {
                    return NoContent();
                }

                var json = JsonConvert.SerializeObject(data, Formatting.Indented);

                return Ok(json);
            }
            catch (ApiException a)
            {
                _logger.LogError($"BaseController.GetJsonResponse ApiException: {a}");
                return Problem(a.Message, a.InnerException?.Message, a.StatusCode);
            }
            catch (Exception e)
            {
                _logger.LogError($"BaseController.GetJsonResponse Exception: {e}");
                return Problem(e.Message, e.InnerException?.Message, 500);
            }
        }
        internal  async Task<TestResult> GetTestResponse<ActionResult>(Func<Task<ActionResult<string>>> func, string endPoint)
        {
            var testResult = new TestResult { EndPoint = endPoint };
            Stopwatch sw = new();

            sw.Start();
            try
            {
                var data = await func();
                sw.Stop();
                if (data == null)
                {
                    testResult.StatusCode = 404;
                }
                else if ((data.Result as NoContentResult) != null)
                {
                    testResult.StatusCode = 204;
                }
                else if ((data.Result as UnauthorizedObjectResult) != null)
                {
                    testResult.StatusCode = (data.Result as UnauthorizedObjectResult).StatusCode;
                }
                else if ((data.Result as ObjectResult) != null)
                {
                    testResult.StatusCode = (data.Result as ObjectResult).StatusCode;
                }
                else
                {
                    // let see what this throws...
                    testResult.StatusCode = 600;
                }

                // let see what this throws...
                // conversion to cause chaos/error
                var json = JsonConvert.SerializeObject(data, Formatting.Indented);

                return testResult;
            }
            catch (ApiException a)
            {
                _logger.LogError($"BaseController.GetTestResponse ApiException: {a}");
                testResult.StatusCode = a.StatusCode;
            }
            catch (Exception e)
            {
                _logger.LogError($"BaseController.GetTestResponse Exception: {e}");

                testResult.StatusCode = 500;
            }
            finally
            {
                testResult.Duration = sw.Elapsed.ToString();
            }
            return testResult;
        }

    }
}
