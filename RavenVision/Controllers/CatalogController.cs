﻿
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace RavenVision.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatalogController : BaseController<CatalogController>
    {
        static readonly string[] scopeRequiredByApi = new string[] { "access_as_user" };

        public CatalogController(IConfiguration iconfig, ILogger<CatalogController> logger) : base(iconfig, "Catalog/", logger)
        {
        }

        [HttpGet("CotsPurchasesByMonth")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetCotsPurchasesByMonth()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("CatalogController.ServicesByTaskOrder Get Invoked");

            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("CotsPurchasesByMonth", CancellationToken.None));
        }

        [HttpGet("CotsByTaskOrder")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetCotsByTaskOrder()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("CatalogController.GetCotsByTaskOrder Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("CotsByTaskOrder", CancellationToken.None));
        }

        [HttpGet("CotsPurchaseDetails/{taskOrderId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetCotsPurchaseDetails(string taskOrderId)
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("CatalogController.CotsPurchaseDetails Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("CotsPurchaseDetails", "taskOrderId", taskOrderId, CancellationToken.None));
        }


        [HttpGet("CotsPurchaseDetails/all")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetCotsPurchaseDetailsAll()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("CatalogController.GetCotsPurchaseDetailsAll Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("CotsPurchaseDetails/all", CancellationToken.None));
        }

        [HttpGet("CotsSummary")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetCotsSummary()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("CatalogController.GetCotsSummary Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("CotsSummary", CancellationToken.None));
        }

        [HttpGet("PurchasesByContract")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetPurchasesByContract()
        {
          //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("CatalogController.PurchasesByContract Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("PurchasesByContract", CancellationToken.None));
        }

        [HttpGet("ServicesByTaskOrder")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetServicesByTaskOrder()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("CatalogController.ServicesByTaskOrder Get Invoked");

            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("ServicesByTaskOrder", CancellationToken.None));
        }

        [HttpGet("ServicesPurchasesByMonth")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetServicesPurchasesByMonth()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("CatalogController.ServicesPurchasesByMonth Get Invoked");

            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("ServicesPurchasesByMonth", CancellationToken.None));
        }

        [HttpGet("ServicePurchaseDetails/{taskOrderId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetServicePurchaseDetails(string taskOrderId)
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("CatalogController.ServicePurchaseDetails Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("ServicePurchaseDetails", "taskOrderId", taskOrderId, CancellationToken.None));
        }


        [HttpGet("ServicePurchaseDetails/all")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetServicePurchaseDetailsAll()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("CatalogController.ServicePurchaseDetails/All Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("ServicePurchaseDetails/all", CancellationToken.None));
        }

        [HttpGet("ServiceSummary")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetServiceSummary()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("CatalogController.ServiceSummary Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("ServicesSummary", CancellationToken.None));
        }

        [HttpGet("Export/CSV")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetCSV()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("CatalogController.CSV Get Invoked");
            return await GetHttpResponse<Task<ActionResult<string>>>(async () => await dataSource.CSV("Catalog/Export/Catalog/CSV", CancellationToken.None));
        }

        [HttpGet("Export/XLM")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetXLM()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("CatalogController.XLM Get Invoked");
            return await GetHttpResponse<Task<ActionResult<string>>>(async () => await dataSource.XML("Catalog/Export/Catalog/XML", CancellationToken.None));
        }
    }
}
