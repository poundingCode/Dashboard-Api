﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.Graph;
using Microsoft.Identity.Web;
using RavenVision.Models;
using System.IO;

namespace RavenVision.Controllers.Graph
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhotoController : ControllerBase
    {
        /// <summary>
        /// The Web API will only accept tokens 1) for users, and 
        /// 2) having the access_as_user scope for this API
        /// </summary>
        static readonly string[] scopeRequiredByApi = new string[] { "access_as_user" };

        public PhotoController(ProfileContext context, ITokenAcquisition tokenAcquisition, GraphServiceClient graphServiceClient, IOptions<MicrosoftGraphOptions> graphOptions)
        {
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public FileResult Photo(string id)
        {
            var profilePhotoPath = Path.Combine(System.IO.Directory.GetCurrentDirectory() + "\\wwwroot\\Images\\", "profilePhoto_" + id + ".jpg");
            if (System.IO.File.Exists(profilePhotoPath))
            {
                return PhysicalFile(profilePhotoPath, "Image/jpg");
            }
            return null;
        }
    }
}
