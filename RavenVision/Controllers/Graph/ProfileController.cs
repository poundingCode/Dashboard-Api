﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
// This class demonstrates 3 ways to get user data.

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Graph;
using Microsoft.Identity.Client;
using Microsoft.Identity.Web;
using Microsoft.Identity.Web.Resource;
using Newtonsoft.Json;
using RavenVision.Models;
using RavenVision.Services;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RavenVision.Controllers.Graph
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProfileController : Controller
    {
        /// <summary>
        /// The Web API will only accept tokens 
        /// 1) for users, and 
        /// 2) having the access_as_user scope for this API
        /// see https://github.com/Azure-Samples/ms-identity-javascript-angular-tutorial/tree/main/3-Authorization-II/1-call-api 
        /// </summary>
        static readonly string[] scopeRequiredByApi = new string[] { "access_as_user" };
        private readonly ILogger<ProfileController> _logger;

        private readonly ProfileContext _context;
        private readonly ITokenAcquisition _tokenAcquisition;
        private readonly GraphServiceClient _graphServiceClient;
        private readonly IOptions<MicrosoftGraphOptions> _graphOptions;

        readonly ITokenAcquisition tokenAcquisition;
        private readonly IGraphApiOperations graphApiOperations;

        public ProfileController(ProfileContext context, ITokenAcquisition tokenAcquisition, IGraphApiOperations graphApiOperations, GraphServiceClient graphServiceClient, IOptions<MicrosoftGraphOptions> graphOptions, ILogger<ProfileController> logger)
        {
            _context = context;
            _tokenAcquisition = tokenAcquisition;
            _graphServiceClient = graphServiceClient;
            _graphOptions = graphOptions;
            _logger = logger;
            this.tokenAcquisition = tokenAcquisition;
            this.graphApiOperations = graphApiOperations;
        }


        [HttpGet()]
        [AuthorizeForScopes(Scopes = new[] { Scopes.UserRead })]
        public async Task<IActionResult> Profile()
        {
            try
            {
                var accessToken = await tokenAcquisition.GetAccessTokenForUserAsync(new[] { Scopes.UserRead });

                var me = await graphApiOperations.GetUserInformation(accessToken);
                var photo = await graphApiOperations.GetPhotoAsBase64Async(accessToken);

                ViewData["Me"] = me;
                ViewData["Photo"] = photo;

                return View();
            }
            catch (MsalException ex)
            {
                HttpContext.Response.ContentType = "application/json";
                HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("An authentication error occurred while acquiring a token for downstream API\n" + ex.ErrorCode + "\n" + ex.Message));

                ViewData["Error"] = ex;

                return View();
            }
            catch (Exception ex)
            {
                ViewData["Error"] = ex;

                return View();
            }
        }

        // GET: api/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult<ProfileItem>> GetProfileItem(string id)
        {
            HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return BadRequest();
                }

                var profileItem = await _context.ProfileItems.FindAsync(id);

                if (profileItem == null)
                {
                    User profile = await _graphServiceClient.Me.Request().GetAsync();
                    if (profile == null)
                    {
                        return NotFound();
                    }

                    profileItem = new ProfileItem
                    {
                        GivenName = profile.GivenName,
                        Id = id,
                        JobTitle = profile.JobTitle,
                        MobilePhone = profile.MobilePhone,
                        Surname = profile.Surname,
                        UserPrincipalName = profile.UserPrincipalName,
                        ManagerName = profile.Manager != null ? ((User)profile.Manager).DisplayName : null,
                    };
                }

                // fire and forget
                await GetProfilePhoto(id);
                return Ok(profileItem);
            }

            catch (Exception ex)
            {
                ViewData["Error"] = ex;

                return Problem(null, null, null, "GetProfileItem Error", null);
            }
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult<ProfileItem>> PostProfileItem(ProfileItem profileItem)
        {
            HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);

            profileItem.FirstLogin = false;

            // This is a synchronous call, so that the clients know, when they call Get, that the 
            // call to the downstream API (Microsoft Graph) has completed.
            try
            {
                User profile = await _graphServiceClient.Me.Request().GetAsync();

                profileItem.Id = profile.Id;
                profileItem.UserPrincipalName = profile.UserPrincipalName;
                profileItem.GivenName = profile.GivenName;
                profileItem.Surname = profile.Surname;
                profileItem.JobTitle = profile.JobTitle;
                profileItem.MobilePhone = profile.MobilePhone;
                profileItem.PreferredLanguage = profile.PreferredLanguage;
            }
            catch (MsalException ex)
            {
                HttpContext.Response.ContentType = "application/json";
                HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("An authentication error occurred while acquiring a token for downstream API\n" + ex.ErrorCode + "\n" + ex.Message));
            }
            catch (Exception ex)
            {
                if (ex.InnerException is MicrosoftIdentityWebChallengeUserException challengeException)
                {
                     _tokenAcquisition.ReplyForbiddenWithWwwAuthenticateHeader(_graphOptions.Value.Scopes.Split(' '), challengeException.MsalUiRequiredException);
                    HttpContext.Response.ContentType = "application/json";
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("interaction required"));
                }
                else
                {
                    HttpContext.Response.ContentType = "application/json";
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("An error occurred while calling the downstream API\n" + ex.Message));
                }
            }

            _context.ProfileItems.Add(profileItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProfileItem", new { id = profileItem.Id }, profileItem);
        }

        // PUT: api/ProfileItems/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProfileItem(string id, ProfileItem profileItem)
        {
            HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);

            if (id != profileItem.Id)
            {
                return BadRequest();
            }

            _context.Entry(profileItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProfileItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        private bool ProfileItemExists(string id)
        {
            return _context.ProfileItems.Any(e => e.Id == id);
        }

        // todo: move image storage to https://ravenvision.blob.core.windows.net/images/, not under wwwroot.
        private async Task GetProfilePhoto(string id)
        {
            try
            {

                var profilePhotoPath = Path.Combine(System.IO.Directory.GetCurrentDirectory() + "\\wwwroot\\Images\\", "profilePhoto_" + id + ".jpg");
                if (!System.IO.File.Exists(profilePhotoPath))
                {
                    var resultUserPhotoFile = await _graphServiceClient.Me.Photo.Content.Request().GetAsync();

                    if (resultUserPhotoFile != null)
                    {
                        // create the file
                        using FileStream profilePhotoFile = System.IO.File.Create(profilePhotoPath);
                        resultUserPhotoFile.Seek(0, SeekOrigin.Begin);
                        resultUserPhotoFile.CopyTo(profilePhotoFile);
                    }
                }
            }
            catch (Exception e)
            {
                if (!e.Message.Contains("ImageNotFound"))
                {
                    _logger.LogError(e.ToString());
                }
            }
        }
    }
}
