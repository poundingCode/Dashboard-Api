﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.Graph;
using Microsoft.Identity.Client;
using Microsoft.Identity.Web;
using Microsoft.Identity.Web.Resource;
using Newtonsoft.Json;
using RavenVision.Model.Graph.SharePoint;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace RavenVision.Controllers.Graph
{
    [Authorize]
    [Route("api/graph/[controller]")]
    [ApiController]
    public class SharePointController : ControllerBase
    {
        // The Web API will only accept tokens 1) for users, and 
        // 2) having the access_as_user scope for this API
        static readonly string[] scopeRequiredByApi = new string[] { "access_as_user" };
        private readonly ITokenAcquisition _tokenAcquisition;
        private readonly GraphServiceClient graphClient;
        private readonly IOptions<MicrosoftGraphOptions> _graphOptions;

        public SharePointController(ITokenAcquisition tokenAcquisition, GraphServiceClient graphServiceClient, IOptions<MicrosoftGraphOptions> graphOptions)
        {
            _tokenAcquisition = tokenAcquisition;
            graphClient = graphServiceClient;
            _graphOptions = graphOptions;
        }

        // GET: api/sites
        [HttpGet("sites/{id}")]
        public async Task<ActionResult<List<SiteVm>>> GetSites(string id = "root")
        {
            try
            {
                HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);

                var list = await GetSubSites(id);
                return list;
            }
            catch (MsalException ex)
            {
                HttpContext.Response.ContentType = "application/json";
                HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("An authentication error occurred while acquiring a token for downstream API\n" + ex.ErrorCode + "\n" + ex.Message));
            }
            catch (Exception ex)
            {
                if (ex.InnerException is MicrosoftIdentityWebChallengeUserException challengeException)
                {
                    _tokenAcquisition.ReplyForbiddenWithWwwAuthenticateHeader(_graphOptions.Value.Scopes.Split(' '), challengeException.MsalUiRequiredException);
                    HttpContext.Response.ContentType = "application/json";
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("interaction required"));
                }
                else
                {
                    HttpContext.Response.ContentType = "application/json";
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("An error occurred while calling the downstream API\n" + ex.Message));
                }
            }
            return null;
        }

        // GET: api/sites
        [HttpGet("drives/{id}")]
        public async Task<ActionResult<List<DriveVm>>> GetDrives(string id)
        {
            try
            {
                HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);

                var list = await GetSiteDrives(id);
                return list;
            }
            catch (MsalException ex)
            {
                HttpContext.Response.ContentType = "application/json";
                HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("An authentication error occurred while acquiring a token for downstream API\n" + ex.ErrorCode + "\n" + ex.Message));
            }
            catch (Exception ex)
            {
                if (ex.InnerException is MicrosoftIdentityWebChallengeUserException challengeException)
                {
                    _tokenAcquisition.ReplyForbiddenWithWwwAuthenticateHeader(_graphOptions.Value.Scopes.Split(' '), challengeException.MsalUiRequiredException);
                    HttpContext.Response.ContentType = "application/json";
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("interaction required"));
                }
                else
                {
                    HttpContext.Response.ContentType = "application/json";
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("An error occurred while calling the downstream API\n" + ex.Message));
                }
            }
            return null;
        }

        private async Task<List<SiteVm>> GetSubSites(string site)
        {
            var lists = await graphClient.Sites[site].Sites
            .Request()
            .GetAsync();
            return Convert(lists);
        }

        /// <summary>
        /// Gets the document libraries of the given site
        /// </summary>
        /// <param name="site"></param>
        /// <returns></returns>
        private async Task<List<DriveVm>> GetSiteDrives(string site)
        {
            ISiteDrivesCollectionPage drives;

            if (site == "root")
            {
                drives = await graphClient.Sites["root"].Drives
               .Request()
               .GetAsync();

                return Convert(drives);
            }

            // return await GetChildren(site);
            //return await this.GetSiteDriveItems(site);

            drives = await graphClient.Sites["root"].Sites[site].Drives
            .Request()
            .GetAsync();
            return Convert(drives);
        }

        private async Task<List<SiteVm>> GetChildren(string site)
        {
            var driveItems = await graphClient.Sites["root"].Sites[site].Drive.Root.Children
            .Request()
            .GetAsync();
            return Convert(driveItems);
        }

        /// <summary>
        /// Get Drive item children
        /// </summary>
        /// <param name="driveId"></param>
        /// <returns></returns>
        // https://docs.microsoft.com/en-us/graph/api/driveitem-list-children?view=graph-rest-1.0&tabs=csharp
        /// drives /{ drive - id}/ items /{ item - id}/ children
        private async Task<List<SiteVm>> GetSiteDriveItems(string driveId)
        {
            var lists = await graphClient.Sites["root"].Drives[driveId].Items //["{driveItem-id}"].Children
           .Request()
           .GetAsync();
            return Convert(lists);
        }
        private List<SiteVm> Convert(ISiteSitesCollectionPage sitesCollection)
        {
            var sites = new List<SiteVm>();
            foreach (var item in sitesCollection)
            {
                var site = new SiteVm
                {
                    Id = item.Id,
                    Name = item.Name,
                    DisplayName = item.DisplayName
                };
                sites.Add(site);
            }
            return sites;
        }

        private List<DriveVm> Convert(ISiteDrivesCollectionPage drives)
        {
            var sites = new List<DriveVm>();
            foreach (var drive in drives)
            {
                var site = new DriveVm
                {
                    Id = drive.Id,
                    Name = drive.Name,
                    Description = drive.Description,
                    LastModifiedDateTime = drive.LastModifiedDateTime,
                    CreatedDateTime = drive.CreatedDateTime,
                };

                sites.Add(site);
            }
            return sites;
        }

        private List<SiteVm> Convert(IDriveItemChildrenCollectionPage driveItems)
        {
            var sites = new List<SiteVm>();
            foreach (var drive in driveItems)
            {
                var site = new SiteVm
                {
                    Id = drive.Id,
                    Name = drive.Name,
                    Description = drive.Description,
                    LastModifiedDateTime = drive.LastModifiedDateTime,
                    CreatedDateTime = drive.CreatedDateTime,
                    CreatedByUser = drive.CreatedBy.User.DisplayName
                };
                sites.Add(site);
            }
            return sites;
        }
        private List<SiteVm> Convert(ISiteListsCollectionPage drives)
        {
            var sites = new List<SiteVm>();
            foreach (var drive in drives)
            {
                var site = new SiteVm
                {
                    Id = drive.Id,
                    Name = drive.Name,
                    Description = drive.Description,
                    LastModifiedDateTime = drive.LastModifiedDateTime,
                    CreatedDateTime = drive.CreatedDateTime,
                    CreatedByUser = drive.CreatedBy.User.DisplayName
                };
                sites.Add(site);
            }
            return sites;
        }

        private List<SiteVm> Convert(IDriveItemsCollectionPage drives)
        {
            var sites = new List<SiteVm>();
            foreach (var drive in drives)
            {
                var site = new SiteVm
                {
                    Id = drive.Id,
                    Name = drive.Name,
                    Description = drive.Description,
                    LastModifiedDateTime = drive.LastModifiedDateTime,
                    CreatedDateTime = drive.CreatedDateTime,
                    CreatedByUser = drive.CreatedBy.User.DisplayName
                };
                sites.Add(site);
            }
            return sites;
        }
    }
}
