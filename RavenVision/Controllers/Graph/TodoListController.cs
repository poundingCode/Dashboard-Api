﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.Graph;
using Microsoft.Identity.Client;
using Microsoft.Identity.Web;
using Microsoft.Identity.Web.Resource;
using Newtonsoft.Json;
using RavenVision.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RavenVision.Controllers.Graph
{
    [Authorize]
    [Route("api/graph/[controller]")]
    [ApiController]
    public class TodoListController : ControllerBase
    {
        // The Web API will only accept tokens 1) for users, and 
        // 2) having the access_as_user scope for this API
        static readonly string[] scopeRequiredByApi = new string[] { "access_as_user" };

        private readonly TodoContext _context;
        private readonly ITokenAcquisition _tokenAcquisition;
        private readonly GraphServiceClient _graphServiceClient;
        private readonly IOptions<MicrosoftGraphOptions> _graphOptions;

        public TodoListController(TodoContext context, ITokenAcquisition tokenAcquisition, GraphServiceClient graphServiceClient, IOptions<MicrosoftGraphOptions> graphOptions)
        {
            _context = context;
            _tokenAcquisition = tokenAcquisition;
            _graphServiceClient = graphServiceClient;
            _graphOptions = graphOptions;
        }
        // GET: api/TodoItems
        [HttpGet]
        public async Task<ActionResult<List<TodoSummary>>> GetTodoItems()
        {
            try
            {
                HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);

                var taskList = await GetToDoTaskList();
                var tasks = await GetTodos(taskList);
                return Convert(tasks);
            }
            catch (MsalException ex)
            {
                HttpContext.Response.ContentType = "application/json";
                HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("An authentication error occurred while acquiring a token for downstream API\n" + ex.ErrorCode + "\n" + ex.Message));
            }
            catch (Exception ex)
            {
                if (ex.InnerException is MicrosoftIdentityWebChallengeUserException challengeException)
                {
                    _tokenAcquisition.ReplyForbiddenWithWwwAuthenticateHeader(_graphOptions.Value.Scopes.Split(' '), challengeException.MsalUiRequiredException);
                    HttpContext.Response.ContentType = "application/json";
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("interaction required"));
                }
                else
                {
                    HttpContext.Response.ContentType = "application/json";
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("An error occurred while calling the downstream API\n" + ex.Message));
                }
            }
            return null;
        }

        private List<TodoSummary> Convert(List<TodoTask> tasks)
        {
            var items = new List<TodoSummary>();
            var i = 0;
            foreach (var task in tasks)
            {
                if (i > 4)
                {
                    break;
                }
                var item = new TodoSummary
                {
                    //Id = task.Id, 
                    Description = task.Title,
                    Status = task.Status.Value.ToString(),
                    DueDateTime = task.DueDateTime,
                    Importance = task.Importance.Value.ToString()
                };
                items.Add(item);
                i++;
            }

            return items;
        }

        private async Task<List<TodoTaskList>> GetToDoTaskList()
        {
            ITodoListsCollectionPage todoLists = new TodoListsCollectionPage();
            var currentLists = new List<TodoTaskList>();
            var todoListsPage = await _graphServiceClient.Me.Todo.Lists.Request().GetAsync();
            currentLists.AddRange(todoListsPage.CurrentPage.ToList());
            while (todoListsPage.NextPageRequest != null)
            {
                todoLists = await todoListsPage.NextPageRequest.GetAsync();
                currentLists.AddRange(todoLists.CurrentPage.ToList());
            }
            return currentLists;
        }

        private async Task<List<TodoTask>> GetTodos(List<TodoTaskList> taskLists)
        {
            var todoTasks = new List<TodoTask>();
            foreach (var todoList in taskLists)
            {
                ;
                var todoTasksPage = await _graphServiceClient.Me.Todo.Lists[todoList.Id].Tasks.Request().GetAsync();
                todoTasks.AddRange(todoTasksPage.CurrentPage.ToList());
                while (todoTasksPage.NextPageRequest != null)
                {
                    todoTasksPage = await todoTasksPage.NextPageRequest.GetAsync();
                    todoTasks.AddRange(todoTasksPage.CurrentPage.ToList());
                }
            }

            return todoTasks;
        }
        private bool TodoItemExists(string id)
        {
            return _context.TodoSummaryItems.Any(e => e.Id == id);
        }

        //private async Task<Dictionary<string, List<TodoTask>>> GetTodos(List<TodoTaskList> taskLists)
        //{
        //    var todoTasksDict = new Dictionary<string, List<TodoTask>>();
        //    foreach (var todoList in taskLists)
        //    {
        //        var todoTasks = new List<TodoTask>();
        //        var todoTasksPage = await _graphServiceClient.Me.Todo.Lists[todoList.Id].Tasks.Request().GetAsync();
        //        todoTasks.AddRange(todoTasksPage.CurrentPage.ToList());
        //        while (todoTasksPage.NextPageRequest != null)
        //        {
        //            todoTasksPage = await todoTasksPage.NextPageRequest.GetAsync();
        //            todoTasks.AddRange(todoTasksPage.CurrentPage.ToList());
        //        }
        //        todoTasksDict[todoList.DisplayName] = todoTasks;
        //    }

        //    return todoTasksDict;
        //}

        // todo: (need to consolidate todoItem w/TodoTask
        //// GET: api/TodoItems/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<TodoItem>> GetTodoItem(int id)
        //{
        //   HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);

        //    var todoItem = await _context.TodoItems.FindAsync(id);

        //    if (todoItem == null)
        //    {
        //        return NotFound();
        //    }

        //    return todoItem;
        //}

        //// PUT: api/TodoItems/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for
        //// more details see https://aka.ms/RazorPagesCRUD.
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutTodoItem(int id, TodoItem todoItem)
        //{
        //   HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);

        //    if (id != todoItem.Id)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(todoItem).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!TodoItemExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        //// POST: api/TodoItems
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for
        //// more details see https://aka.ms/RazorPagesCRUD.
        //[HttpPost]
        //public async Task<ActionResult<TodoItem>> PostTodoItem(TodoItem todoItem)
        //{
        //   HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
        //    string owner = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
        //    todoItem.Owner = owner;
        //    todoItem.Status = Microsoft.Graph.TaskStatus.NotStarted;

        //    _context.TodoItems.Add(todoItem);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetTodoItem", new { id = todoItem.Id }, todoItem);
        //}

        //// DELETE: api/TodoItems/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<TodoItem>> DeleteTodoItem(int id)
        //{
        //   HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);

        //    var todoItem = await _context.TodoItems.FindAsync(id);
        //    if (todoItem == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.TodoItems.Remove(todoItem);
        //    await _context.SaveChangesAsync();

        //    return todoItem;
        //}
    }
}
