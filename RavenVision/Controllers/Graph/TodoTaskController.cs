﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.Graph;
using Microsoft.Identity.Client;
using Microsoft.Identity.Web;
using Microsoft.Identity.Web.Resource;
using Newtonsoft.Json;
using RavenVision.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RavenVision.Controllers.Graph
{
    [Authorize]
    [Route("api/graph/[controller]")]
    [ApiController]
    public class TodoTaskController : ControllerBase
    {
        // The Web API will only accept tokens 1) for users, and 
        // 2) having the access_as_user scope for this API
        static readonly string[] scopeRequiredByApi = new string[] { "access_as_user" };
        private readonly ITokenAcquisition _tokenAcquisition;
        private readonly GraphServiceClient _graphServiceClient;
        private readonly IOptions<MicrosoftGraphOptions> _graphOptions;

        public TodoTaskController(TodoContext context, ITokenAcquisition tokenAcquisition, GraphServiceClient graphServiceClient, IOptions<MicrosoftGraphOptions> graphOptions)
        {
            _tokenAcquisition = tokenAcquisition;
            _graphServiceClient = graphServiceClient;
            _graphOptions = graphOptions;
        }

        // GET: api/TodoItems
        [HttpGet("TodoTaskList")]
        public async Task<ActionResult<List<TodoSummary>>> GetTodoTaskList()
        {
            try
            {
                HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);

                var taskList = await GetToDoTaskList();
                var tasks = await GetTodos(taskList);
                return Convert(tasks);
            }
            catch (MsalException ex)
            {
                HttpContext.Response.ContentType = "application/json";
                HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("An authentication error occurred while acquiring a token for downstream API\n" + ex.ErrorCode + "\n" + ex.Message));
            }
            catch (Exception ex)
            {
                if (ex.InnerException is MicrosoftIdentityWebChallengeUserException challengeException)
                {
                    _tokenAcquisition.ReplyForbiddenWithWwwAuthenticateHeader(_graphOptions.Value.Scopes.Split(' '), challengeException.MsalUiRequiredException);
                    HttpContext.Response.ContentType = "application/json";
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("interaction required"));
                }
                else
                {
                    HttpContext.Response.ContentType = "application/json";
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("An error occurred while calling the downstream API\n" + ex.Message));
                }
            }
            return null;
        }

        private async Task<List<TodoTaskList>> GetToDoTaskList()
        {
            ITodoListsCollectionPage todoLists = new TodoListsCollectionPage();
            var currentLists = new List<TodoTaskList>();
            var todoListsPage = await _graphServiceClient.Me.Todo.Lists.Request().GetAsync();
            currentLists.AddRange(todoListsPage.CurrentPage.ToList());
            while (todoListsPage.NextPageRequest != null)
            {
                todoLists = await todoListsPage.NextPageRequest.GetAsync();
                currentLists.AddRange(todoLists.CurrentPage.ToList());
            }
            return currentLists;
        }

        private async Task<List<TodoTask>> GetTodos(List<TodoTaskList> taskLists)
        {
            var todoTasks = new List<TodoTask>();
            foreach (var todoList in taskLists)
            {
                ;
                var todoTasksPage = await _graphServiceClient.Me.Todo.Lists[todoList.Id].Tasks.Request().GetAsync();
                todoTasks.AddRange(todoTasksPage.CurrentPage.ToList());
                while (todoTasksPage.NextPageRequest != null)
                {
                    todoTasksPage = await todoTasksPage.NextPageRequest.GetAsync();
                    todoTasks.AddRange(todoTasksPage.CurrentPage.ToList());
                }
            }

            return todoTasks;
        }

        private List<TodoSummary> Convert(List<TodoTask> tasks)
        {
            var items = new List<TodoSummary>();
            var i = 0;
            foreach (var task in tasks)
            {
                if (i > 4)
                {
                    break;
                }
                var item = new TodoSummary
                {
                    //Id = task.Id, 
                    Description = task.Title,
                    Status = task.Status.Value.ToString(),
                    DueDateTime = task.DueDateTime,
                    Importance = task.Importance.Value.ToString()
                };
                items.Add(item);
                i++;
            }

            return items;
        }
    }
}
