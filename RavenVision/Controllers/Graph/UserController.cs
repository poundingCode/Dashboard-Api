﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.Graph;
using Microsoft.Identity.Client;
using Microsoft.Identity.Web;
using Microsoft.Identity.Web.Resource;
using Newtonsoft.Json;
using RavenVision.Model.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RavenVision.Controllers.Graph
{
    [Authorize]
    [Route("api/graph/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        // The Web API will only accept tokens 1) for users, and 
        // 2) having the access_as_user scope for this API
        static readonly string[] scopeRequiredByApi = new string[] { "access_as_user" };
        private readonly ITokenAcquisition _tokenAcquisition;
        private readonly GraphServiceClient _graphServiceClient;
        private readonly IOptions<MicrosoftGraphOptions> _graphOptions;

        public UserController(ITokenAcquisition tokenAcquisition, GraphServiceClient graphServiceClient, IOptions<MicrosoftGraphOptions> graphOptions)
        {
            _tokenAcquisition = tokenAcquisition;
            _graphServiceClient = graphServiceClient;
            _graphOptions = graphOptions;
        }

        // GET: api/GetUsers
        [HttpGet("users")]
        public async Task<ActionResult<ICollection<UserSummary>>> GetUsers()
        {
            try
            {
                HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);

                var userList = await GetUsersAndManagers();
                var u = Convert(userList);
                return Ok(u);
            }
            catch (MsalException ex)
            {
                HttpContext.Response.ContentType = "application/json";
                HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("An authentication error occurred while acquiring a token for downstream API\n" + ex.ErrorCode + "\n" + ex.Message));
            }
            catch (Exception ex)
            {
                if (ex.InnerException is MicrosoftIdentityWebChallengeUserException challengeException)
                {
                    _tokenAcquisition.ReplyForbiddenWithWwwAuthenticateHeader(_graphOptions.Value.Scopes.Split(' '), challengeException.MsalUiRequiredException);
                    HttpContext.Response.ContentType = "application/json";
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("interaction required"));
                }
                else
                {
                    HttpContext.Response.ContentType = "application/json";
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("An error occurred while calling the downstream API\n" + ex.Message));
                }
            }
            return null;
        }

        private async Task<List<User>> GetUsersAndManagers()
        {
            IGraphServiceUsersCollectionPage collectionPage = new GraphServiceUsersCollectionPage();
            var currentList = new List<User>();

            collectionPage = await _graphServiceClient.Users.Request().Expand("manager").GetAsync();
            // todo: remove magic string
            currentList.AddRange(collectionPage.CurrentPage.ToList());
            while (collectionPage.NextPageRequest != null)
            {
                collectionPage = await collectionPage.NextPageRequest.GetAsync();
                currentList.AddRange(collectionPage.CurrentPage.ToList());
            }


            return currentList;
        }

        private List<UserSummary> Convert(List<User> users)
        {
            var items = new List<UserSummary>();
            foreach (var u in users) // .Where(x => x.Manager != null)
            {
                var item = new UserSummary
                {
                    Id = u.Id,
                    DisplayName = u.DisplayName,
                    GivenName = u.GivenName,
                    JobTitle = u.JobTitle,
                    Mail = u.Mail,
                    // Manager = u.Manager,
                    // ManagerName = ((User)u.Manager).DisplayName
                };
                items.Add(item);
            }
            return items.OrderBy(m => m.ManagerName).ThenBy(d => d.DisplayName).ToList();
        }
    }
}
