﻿using Microsoft.AspNetCore.Mvc;

namespace RavenVision.Controllers
{
    [Route("/")]
    [Controller]
    public class HomeController : Controller
    {
        public HomeController()
        {
        }

        [HttpGet()]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet("Home")]
        public ActionResult Home()
        {
            return View();
        }

        [HttpGet("Privacy")]
        public ActionResult Privacy()
        {
            return View();
        }
    }
}
