using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RavenVision.DataSources.API;

namespace RavenVision.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NetworkController : BaseController<NetworkController>
    {
        static readonly string[] scopeRequiredByApi = new string[] { "access_as_user" };

        public NetworkController(IConfiguration iconfig, ILogger<NetworkController> logger) : base(iconfig, "network/", logger)
        {
        }

        [HttpGet("CapacityAndUtilizationLatest")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetCapacityAndUtilizationLatest()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.CapacityAndUtilizationLatest Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("CapacityAndUtilizationLatest", CancellationToken.None));
        }

        [HttpGet("capandutilrollup")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetCapAndUtilRollup()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.GetCapAndUtilRollup Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("capandutilrollup", CancellationToken.None));
        }

        [HttpGet("Capandutilrollup/physical")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetCapAndUtilRollupPhysical()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.GetCapAndUtilRollupPhysical Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("capandutilrollup/physical", CancellationToken.None));
        }

        [HttpGet("Capandutilrollup/virtual")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetCapAndUtilRollupVirtual ()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.CapacityAndUtilizationLatest Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("Capandutilrollup/virtual", CancellationToken.None));
        }
        

        [HttpGet("DeviceHealthChart")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetDeviceHealthChart()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.DeviceHealthChart Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("DeviceHealthChart", CancellationToken.None));
        }


        [HttpGet("InterfaceMetrics/{deviceId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetInterfaceMetrics(int deviceId)
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.GetInterfaceMetrics Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"InterfaceMetrics", "deviceId", deviceId.ToString(), CancellationToken.None));
        }

        [HttpGet("InterfaceList")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetInterfaceList()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.GetInterfaceList Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("InterfaceList", CancellationToken.None));
        }


        [HttpGet("DeviceThresholds/{deviceId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetDeviceThresholds(int deviceId)
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.GetDeviceThresholds Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"DeviceThresholds", "deviceId", deviceId.ToString(), CancellationToken.None));
        }

        [HttpGet("DeviceHealth")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetDeviceHealth()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.DeviceHealth Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"DeviceHealth", CancellationToken.None));
        }

        [HttpGet("DeviceHealth/{deviceId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetDeviceHealth(int deviceId)
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.DeviceHealth Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"DeviceHealth/{deviceId}", CancellationToken.None));
        }

        [HttpGet("DeviceHealthByMonth")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetDeviceHealthByMonth()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.DeviceHealth Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"DeviceHealthByMonth", CancellationToken.None));
        }


        [HttpGet("DeviceHealthThreshold/CPU")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetDeviceHealthThresholdCPU()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.GetDeviceHealthThresholdCPU Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"DeviceHealthThreshold/CPU", CancellationToken.None));
        }

        [HttpGet("DeviceHealthThreshold/Mem")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetDeviceHealthThresholdMem()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.GetDeviceHealthThresholdMem Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"DeviceHealthThreshold/Mem", CancellationToken.None));
        }
        [HttpGet("DeviceMetrics/{deviceId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetDeviceMetrics(int deviceId)
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.DeviceMetrics Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"DeviceMetrics/{deviceId}", CancellationToken.None));
        }

        [HttpGet("DeviceList")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetDeviceList()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.DeviceList Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("DeviceList", CancellationToken.None));
        }

        [HttpGet("AvailabilityHistory/{deviceId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetAvailabilityHistory(int deviceId)
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.AvailabilityHistory Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"AvailabilityHistory/{deviceId}", CancellationToken.None));
        }

        [HttpGet("AvailabilityCount")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetAvailabilityCount()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.AvailabilityCount Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"AvailabilityCount", CancellationToken.None));
        }


        [HttpGet("SiLoPowerPackList")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetSiLoPowerPackList()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.SiLoPowerPackList Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("SiLoPowerPackList", CancellationToken.None));
        }

        [HttpPost("NetworkMetrics")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> PostNetworkMetrics()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.NetworkMetrics Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.PostApi("NetworkMetrics", CancellationToken.None));
        }

        [HttpGet("NetworkMetrics")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetNetworkMetrics()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.NetworkMetrics Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("NetworkMetrics", CancellationToken.None));
        }


        [HttpGet("LatencyAverage")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetLatencyAverage()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.LatencyAverage Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("LatencyAverage", CancellationToken.None));
        }

        [HttpGet("DeviceListByClass")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetTaskOrderList()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.DeviceListByClass Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("DeviceListByClass", CancellationToken.None));
        }

        [HttpGet("UnavailableDeviceList")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetUnavailableDeviceList()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("NetworkController.GetUnavailableDeviceList Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("UnavailableDeviceList", CancellationToken.None));
        }
    }
}
