﻿
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace RavenVision.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PurchasingController : BaseController<PurchasingController>
    {
        static readonly string[] scopeRequiredByApi = new string[] { "access_as_user" };

        public PurchasingController(IConfiguration iconfig, ILogger<PurchasingController> logger): base(iconfig, "cost/", logger)
        {
        }

        [HttpGet("Component/Total")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetComponentTotal()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetComponentTotal Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("Component/Total", CancellationToken.None));
        }

        [HttpGet("Component/TaskOrder/Month/Total")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetComponentTaskOrderMonthTotal()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetComponentTaskOrderMonthTotal Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("Component/TaskOrder/Month/Total", CancellationToken.None));
        }

        [HttpGet("Component/TaskOrderTotal")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetComponentTaskOrderTotal()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetComponentTaskOrderTotal Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("Component/TaskOrderTotal", CancellationToken.None));
        }

        [HttpGet("ContractLevelSummaryCosts")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetContractLevelSummaryCosts()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetContractLevelSummaryCosts Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("ContractLevelSummaryCosts", CancellationToken.None));
        }

        [HttpGet("ContractLevelSummaryCosts/Export/CSV")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetContractLevelSummaryCostsExportCSV()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetContractLevelSummaryCostsExportCSV Invoked");
            return await GetHttpResponse<Task<ActionResult<string>>>(async () => await dataSource.CSV("ContractLevelSummaryCosts/Export/CSV", CancellationToken.None));
        }

        [HttpGet("ContractList")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetContractList()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetContractList Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("GetContractList", CancellationToken.None));
        }

        [HttpGet("ContractSpendingVsCeiling")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetContractSpendingVsCeiling()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.ContractSpendingVsCeiling Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("ContractSpendingVsCeiling", CancellationToken.None));
        }

        [HttpGet("IdiqClin/TotalByCategory")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetIdiqClinTotalByCategory()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetIdiqClinTotalByCategory Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("IdiqClin/TotalByCategory", CancellationToken.None));
        }

        [HttpGet("IdiqClin/TotalByCategory/Month")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetIdiqClinTotalByCategoryMonth()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetIdiqClinTotalByCategoryMonth Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("IdiqClin/TotalByCategory/Month", CancellationToken.None));
        }

        [HttpGet("Percentages/AwardedVsContractCeiling")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetPercentagesAwardedVsContractCeiling()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetPercentagesAwardedVsContractCeiling Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("Percentages/AwardedVsContractCeiling", CancellationToken.None));
        }

        [HttpGet("Percentages/FundedVsContractCeiling")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetPercentagesFundedVsContractCeiling()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetPercentagesFundedVsContractCeiling Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("Percentages/FundedVsContractCeiling", CancellationToken.None));
        }

        [HttpGet("Percentages/SpentVsContractCeiling")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetPercentagesSpentVsContractCeiling()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetPercentagesSpentVsContractCeiling Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("Percentages/SpentVsContractCeiling", CancellationToken.None));
        }

        [HttpGet("Resources/Monthly/CSP")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetResourcesMonthlyCSP()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetResourcesMonthlyCSP Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("Resources/Monthly/CSP", CancellationToken.None));
        }


        [HttpGet("Resources/Monthly/CSP/Chart")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetResourcesMonthlyCSPChart()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetResourcesMonthlyCSPChart Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("Resources/Monthly/CSP/Chart", CancellationToken.None));
        }

        [HttpGet("Resources/Monthly/HCE")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetResourcesMonthlyHCE()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetResourcesMonthlyHCE Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("Resources/Monthly/HCE", CancellationToken.None));
        }

        [HttpGet("Resources/Monthly/HCE/Chart")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetResourcesMonthlyHceChart()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetResourcesMonthlyHceChart Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("Resources/Monthly/HCE/Chart", CancellationToken.None));
        }

        [HttpGet("SpaceBased/Export/CSV")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetSpaceBasedExportCSV()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.SpaceBasedExportCSV Invoked");

            return await GetHttpResponse<Task<ActionResult<string>>>(async () => await dataSource.CSV("Export/SpaceBased/CSV", CancellationToken.None));
        }

        [HttpGet("SpaceBased/Export/XML")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetSpaceBasedExportXML()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetSpaceBasedExportXML Invoked");
            return await GetHttpResponse<Task<ActionResult<string>>>(async () => await dataSource.XML("Export/SpaceBased/XML", CancellationToken.None));
        }


        [HttpGet("SpaceBased/Overall")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetSpaceBasedOverall()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.SpaceBasedOverall Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("SpaceBased/Overall", CancellationToken.None));
        }

        [HttpGet("TaskOrderPurchasing")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTaskOrderPurchasing()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetTaskOrderPurchasing Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("TaskOrderPurchasing", CancellationToken.None));
        }


        [HttpGet("TaskOrderPurchasing/ByMonth/Chart")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTaskOrderPurchasingByMonthChart()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetTaskOrderPurchasingByMonthChart Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("taskOrderPurchasingByMonth/Chart", CancellationToken.None));
        }

        [HttpGet("TaskOrderPurchasing/Export/CSV")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTaskOrderPurchasingExportCSV()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetTaskOrderPurchasing Invoked");
            return await GetHttpResponse<Task<ActionResult<string>>>(async () => await dataSource.CSV("TaskOrderPurchasing/Export/CSV", CancellationToken.None));
        }

        [HttpGet("TotalContractExpenditure")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTotalContractExpenditureData()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingControllerTotalContractExpenditures Invoked");

            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("TotalContractExpenditures", CancellationToken.None));
        }

        [HttpGet("TotalContractCAGR")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTotalContractCAGR()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.TotalContractCAGR Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("TotalContractCAGR", CancellationToken.None));
        }

        [HttpGet("TaskOrderSpendingVsCeiling")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTaskOrderSpendingVsCeiling()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.TaskOrderSpendingVsCeiling Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("TaskOrderSpendingVsCeiling", CancellationToken.None));
        }


        [HttpGet("TaskOrderSpendingVsCeiling/Chart")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTaskOrderSpendingVsCeilingChart()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.TaskOrderSpendingVsCeilingChart Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("TaskOrderSpendingVsCeiling/Chart", CancellationToken.None));
        }

        [HttpGet("TotalPurchasing/All")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTotalPurchasingAll()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("TotalPurchasing/All", CancellationToken.None));
        }


        [HttpGet("TotalPurchasing/ByMonthChart")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTotalPurchasingByMonthChart()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetTotalPurchasingByMonthChart Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("TotalPurchasesByMonth/Chart", CancellationToken.None));
        }

        [HttpGet("TotalPurchasing/ByMonth/Cots/Chart")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTotalPurchasingByMonthCotsChart()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetTotalPurchasingByMonthCotsChart Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("TotalPurchasesByMonth/Cots/Chart", CancellationToken.None));
        }

        [HttpGet("TotalPurchasing/ByMonth/Services/Chart")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTotalPurchasingByMonthServicesChart()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetTotalPurchasingByMonthServicesChart Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("TotalPurchasesByMonth/Services/Chart", CancellationToken.None));
        }

        [HttpGet("TotalPurchasing/ByTaskOrderAndServiceCategory")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTotalPurchasingByTaskOrderAndServiceCategory()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("TotalPurchasing/ByTaskOrderAndServiceCategory", CancellationToken.None));
        }



        [HttpGet("TotalPurchasing/TaskOrder")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTotalPurchasingByTaskOrder()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.TotalPurchasingByTaskOrder Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("TotalPurchasing/ByTaskOrder", CancellationToken.None));
        }

        [HttpGet("TotalPurchasing/IdiqClin")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTotalPurchasingByIdiqClin()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetTotalPurchasingByIdiqClin Invoked");

            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("TotalPurchasing/ByIdiqClin", CancellationToken.None));
        }

        [HttpGet("TotalPurchasing/ByIdiqClinCategory")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTotalPurchasingByIdiqClinCategory()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetTotalPurchasingByIdiqClinCategory Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("TotalPurchasing/ByIdiqClinCategory", CancellationToken.None));
        }

        [HttpGet("TaskOrderPurchasing/ContractClin/{taskOrderId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTaskOrderPurchasingByContractClin(int taskOrderId)
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetTaskOrderPurchasingByContractClin Invoked");

            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"TaskOrderPurchasing/ByContractClin/{taskOrderId}", CancellationToken.None));

        }

        [HttpGet("TaskOrderPurchasing/ByIdiqClin/{taskOrderId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTaskOrderPurchasingByIdiqClin(int taskOrderId)
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetTaskOrderPurchasingByIdiqClin Invoked");
            var arg = taskOrderId.ToString();
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"TaskOrderPurchasing/ByIdiqClin/{taskOrderId}", CancellationToken.None));

        }

        [HttpGet("LaborHours/Everything")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetLaborHourSpending_Everything()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetLaborHourSpending_Overall Invoked");

            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("LaborHours/Everything", CancellationToken.None));
        }

        [HttpGet("LaborHours/Export/CSV")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetLaborHourExportCSV()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetLaborHourExportCSV Invoked");

            return await GetHttpResponse<Task<ActionResult<string>>>(async () => await dataSource.CSV("Export/LaborHours/CSV", CancellationToken.None));
        }

        [HttpGet("LaborHours/Export/XML")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetLaborHourExportXML()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetLaborHourExportCSV Invoked");

            return await GetHttpResponse<Task<ActionResult<string>>>(async () => await dataSource.XML("Export/LaborHours/XLM", CancellationToken.None));
        }

        [HttpGet("LaborHours/Overall")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetLaborHourSpending_Overall()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetLaborHourSpending_Overall Invoked");

            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("LaborHours/Overall", CancellationToken.None));
        }

        [HttpGet("LaborHours/OverallByYear")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetLaborHoursOverallByYear()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetLaborHoursOverallByYear Invoked");

            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("LaborHours/OverallByYear", CancellationToken.None));
        }

        [HttpGet("LaborHours/Overall/ForYear/{year}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetLaborHoursOverallForYear(int year)
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetLaborHoursOverallForYear Invoked");

            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"LaborHours/Overall/ForYear/{year}", CancellationToken.None));
        }
                
        [HttpGet("LaborHours/ByYear/{laborCategoryId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetLaborHourSpending_Year(int laborCategoryId)
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetLaborHourSpending_Year Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"LaborHours/ByYear/{laborCategoryId}/", CancellationToken.None));

        }

        [HttpGet("LaborHours/ByQuarter/{laborCategoryId}/{year}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetLaborHourSpending_Quarter(int laborCategoryId, int year)
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetLaborHourSpending_Quarter Invoked");

            var arg1 = laborCategoryId.ToString();
            var arg2 = year.ToString();
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"LaborHours/ByQuarter/{laborCategoryId}/{year}", CancellationToken.None));

        }

        [HttpGet("LaborHours/ByMonth")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetLaborHourSpendingByMonthChart()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetLaborHourSpendingByMonthChart Invoked");

            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("LaborHourSpendingByMonth/Chart", CancellationToken.None));
        }

        [HttpGet("Export/FundingData/TaskOrderClinCategory/CSV")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetExportFundingData_TaskOrderClinCategory_CSV()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetExportFundingData_TaskOrderClinCategory_CSV Invoked");

            return await GetHttpResponse<Task<ActionResult<string>>>(async () => await dataSource.CSV("Export/FundingData/TaskOrderClinCategory/CSV", CancellationToken.None));
        }

        [HttpGet("Export/FundingData/TaskOrderClinCategory/XML")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetExportFundingData_TaskOrderClinCategory_XML()
        {
           //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetExportFundingData_TaskOrderClinCategory_XML Invoked");
            return await GetHttpResponse<Task<ActionResult<string>>>(async () => await dataSource.XML("Export/FundingData/TaskOrderClinCategory/XML", CancellationToken.None));
        }

        [HttpGet("Export/Billing/CSV")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetExportBillingData_CSV()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetExportBillingData_CSVInvoked");
            return await GetHttpResponse<Task<ActionResult<string>>>(async () => await dataSource.CSV("Export/Billing/CSV", CancellationToken.None));
        }

        [HttpGet("Export/Billing/XML")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetExportBillingData_XML()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetExportBillingData_XML Invoked");
            return await GetHttpResponse<Task<ActionResult<string>>>(async () => await dataSource.XML("Export/Billing/XML", CancellationToken.None));
        }

        [HttpGet("Export/Billing_Filter/XML")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetExportBilling_Filter_XML()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetExportBillingData_XML Invoked");
            return await GetHttpResponse<Task<ActionResult<string>>>(async () => await dataSource.XML("Export/Billing_Filter/XML", CancellationToken.None));
        }

        [HttpGet("Export/Billing_Filter/CSV")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetExportilling_Filter_CSV()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetExportBillingData_XML Invoked");
            return await GetHttpResponse<Task<ActionResult<string>>>(async () => await dataSource.XML("Export/Billing_Filter/CSV", CancellationToken.None));
        }

        [HttpGet("TaskOrderList/{contractId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTaskOrderList(int contractId)
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetTaskOrderList Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"GetTaskOrderList/{contractId}", CancellationToken.None));

        }

        [HttpGet("TaskOrderClinList/{taskOrderId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTaskOrderClinListData(int taskOrderId)
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetTaskOrderClinListData Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"GetTaskOrderClinList/{taskOrderId}", CancellationToken.None));
        }

        [HttpGet("TaskOrderClinCategoryList/{taskOrderClinId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetTaskOrderClinCategoryListData(int taskOrderClinId)
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.GetTaskOrderClinCategoryListData Invoked");

            var arg = taskOrderClinId.ToString();
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"GetTaskOrderClinCategoryList/{taskOrderClinId}", CancellationToken.None));
        }

        [HttpGet("TotalSpendingForYear/{year}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetTotalSpendingForYear(int year)
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("PurchasingController.TotalSpendingForYear Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"TotalSpendingForYear/{year}", CancellationToken.None));
        }
    }
}
