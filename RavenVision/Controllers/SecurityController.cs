﻿using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace RavenVision.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SecurityController : BaseController<SecurityController>
    {
        static readonly string[] scopeRequiredByApi = new string[] { "access_as_user" };

        public SecurityController(IConfiguration iconfig, ILogger<SecurityController> logger) : base(iconfig, "security/", logger)
        {
        }

        [HttpGet("Incident/TicketSummary")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetIncidentTicketSummary()
        {
          //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("security.IncidentTicketSummary Invoked");
           return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("Incident/TicketSummary", CancellationToken.None));
        }

        [HttpGet("Incident/All")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetIncidentAll()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("security.GetIncidentAll Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("Incident/All", CancellationToken.None));
        }
        
        [HttpGet("Export/Incident/CSV")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetExportIncidentCSV()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("security.GetExportIncidentCSV Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("Export/Incident/CSV", CancellationToken.None));
        }

        [HttpGet("Export/Incident/XML")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetExportIncidentXML()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("security.ExportIncidentXML Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("Export/Incident/XML", CancellationToken.None));
        }
    }
}