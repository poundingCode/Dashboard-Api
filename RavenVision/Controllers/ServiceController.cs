﻿using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace RavenVision.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceController : BaseController<ServiceController>
    {
        static readonly string[] scopeRequiredByApi = new string[] { "access_as_user" };

        public ServiceController(IConfiguration iconfig, ILogger<ServiceController> logger) : base(iconfig, "", logger)
        {
        }

        [HttpGet("catalog/SlaSummary")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetSlaSummary()
        {
          //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("catalog.SlaSummary Invoked");
           return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("sla/SlaSummary", CancellationToken.None));
        }

        [HttpGet("catalog/SlaScore")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetSlascore()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation($"{this.GetType().Name} Get Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("sla/SlaRollup", CancellationToken.None));
        }
        
        [HttpGet("catalog/SlaFailed")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetSLAFailed()
        {
            //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
            _logger.LogInformation("catalog.SlaFailed Invoked");
            return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi("sla/FailedSla", CancellationToken.None));
        }
    }
}
