﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Web;
using RavenVision.DataSources.API.Snow;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using static RavenVision.Models.Constants;
using Microsoft.Extensions.Logging;
using RavenVision.DataSources.API;
using RavenVision.Model.Snow;
using System.Threading;
using RavenVision.Services;

namespace RavenVision.Controllers
{
    // [Authorize]
    // [RequiredScope("access_as_user")]
    [Route("api/[controller]")]
    [ApiController]
    public class SnowController : BaseController<SnowController>
    {
        private readonly ITokenAcquisition _tokenAcquisition;
         
        private readonly IOboIdentityApi _oboIdentityApi;
        private readonly List<string> _scopesToAccessApi;
        // private readonly string _scopeToAccessApi;

        private readonly IConfiguration _configuration;
        /// <summary>
        /// The Web API will only accept tokens 
        /// 1) for users, and 
        ///  2) having the access_as_user scope for this API
        /// https://login.microsoftonline.us/da8e06a2-ecd4-4f83-a1c3-cdf8f2f75525/oauth2/v2.0/token
        /// https://dccodev-cluster.usgovvirginia.cloudapp.usgovcloudapi.net:8000/user_impersonation
        /// </summary>
        /// <param name="tokenAcquisition"></param>
        /// <param name="dataSource"></param>
        public SnowController(ITokenAcquisition tokenAcquisition, ILogger<SnowController> logger, IOboIdentityApi oboIdentityApi, IConfiguration iconfig) : base(iconfig, "", logger)
        {
            _tokenAcquisition = tokenAcquisition;
            _configuration = iconfig;
            _oboIdentityApi = oboIdentityApi;
            _scopesToAccessApi = new()
            {
                _configuration["Scopes:Snow:Roles"]
            };
        }

        #region HardwareInventoryManagement
        [HttpGet("HardwareInventory/HardwareInventoryManagement")]
        public async Task<ActionResult<string>> GetHardwareInventoryManagement(int skip = 0, int take=100)
        {
            var path = $"{HardwareInventory.ExpiringMaintenanceContracts}&sysparm_limit={take}&sysparm_offset={skip}&sysparm_fields=contract_model,expiration,description,starts,sys_created_on,sys_domain,sys_id,total_cost,vendor";
           // return await _snowService.GetHardwareInventoryManagement(path);

            var json = await _oboIdentityApi.CallWebApiForUserAsync<HardwareInventoryManagement>(Apis.Snow, $"{HardwareInventory.ExpiringMaintenanceContracts}&sysparm_limit={take}&sysparm_offset={skip}&sysparm_fields=contract_model,expiration,description,starts,sys_created_on,sys_domain,sys_id,total_cost,vendor");
            return json;

            // return await GetJsonResponse<Task<ActionResult<string>>>(async () => await dataSource.GetApi($"{HardwareInventory.ExpiringMaintenanceContracts}&sysparm_limit={take}&sysparm_offset={skip}", CancellationToken.None));
        }

        [HttpGet("HardwareInventory/DayAgingByHardwareAsset")]
        public async Task<ActionResult<string>> GetDayAgingByHardwareAsset(int skip = 0, int take = 100)
        {
            var json = await _oboIdentityApi.CallWebApiForUserAsync<HardwareAsset>(Apis.Snow, $"{HardwareInventory.DayAgingByHardwareAsset}?sysparm_limit={take}&sysparm_offset={skip}");
            return json;
        }

        [HttpGet("HardwareInventory/HardwareAsset")]
        public async Task<ActionResult<string>> GetHardwareAssets(int skip = 0, int take = 100)
        {
            var json = await _oboIdentityApi.CallWebApiForUserAsync<HardwareAsset>(Apis.Snow, $"{HardwareInventory.HardwareAssets}?sysparm_limit={take}&sysparm_offset={skip}&sysparm_fields=sys_created_by,sys_class_name,serial_number,sys_created_on,sys_domain,display_name,sys_id,quantity,model_category,model,ci,location,link");
            return json;
        }

        [HttpGet("HardwareInventory/MaintenanceAgreementEndDates")]
        public async Task<ActionResult<string>> GetMaintenanceAgreementEndDates(int skip = 0, int take = 100)
        {
            var json = await _oboIdentityApi.CallWebApiForUserAsync<MaintenanceAgreementEndDate>(Apis.Snow, $"{HardwareInventory.MaintenanceAgreementEndDates}&sysparm_limit={take}&sysparm_offset={skip}");
            return json;
        }
        #endregion

        #region Service Management


        [HttpGet("ServiceManagement/Closed")]
        public async Task<ActionResult<string>> GetClosed(int skip = 0, int take = 100)
        {
            var json = await _oboIdentityApi.CallWebApiForUserAsync<MaintenanceAgreementEndDate>(Apis.Snow, $"{Service.Closed }&sysparm_limit={take}&sysparm_offset={skip}");
            return json;
        }

        [HttpGet("ServiceManagement/Escalated")]
        public async Task<ActionResult<string>> GetEscalated(int skip = 0, int take = 100)
        {
            var json = await _oboIdentityApi.CallWebApiForUserAsync<MaintenanceAgreementEndDate>(Apis.Snow, $"{Service.Escalated }&sysparm_limit={take}&sysparm_offset={skip}");
            return json;
        }
        [HttpGet("ServiceManagement/Initiated")]
        public async Task<ActionResult<string>> GetInitiated(int skip = 0, int take = 100)
        {
            var json = await _oboIdentityApi.CallWebApiForUserAsync<MaintenanceAgreementEndDate>(Apis.Snow, $"{Service.Initiated}&sysparm_limit={take}&sysparm_offset={skip}");
            return json;
        }
        [HttpGet("ServiceManagement/MTTR")]
        public async Task<ActionResult<string>> GetMTTR(int skip = 0, int take = 100)
        {
            var json = await _oboIdentityApi.CallWebApiForUserAsync<MaintenanceAgreementEndDate>(Apis.Snow, $"{Service.MTTR }&sysparm_limit={take}&sysparm_offset={skip}");
            return json;
        }
        [HttpGet("ServiceManagement/OpenLastMonth")]
        public async Task<ActionResult<string>> GetOpenLastMonth(int skip = 0, int take = 100)
        {
            var json = await _oboIdentityApi.CallWebApiForUserAsync<MaintenanceAgreementEndDate>(Apis.Snow, $"{Service.OpenLastMonth}&sysparm_limit={take}&sysparm_offset={skip}");
            return json;
        }
        [HttpGet("ServiceManagement/PreviousMonthDaily")]
        public async Task<ActionResult<string>> GetPreviousMonthDaily(int skip = 0, int take = 100)
        {
            var json = await _oboIdentityApi.CallWebApiForUserAsync<MaintenanceAgreementEndDate>(Apis.Snow, $"{Service.PreviousMonthDaily}&sysparm_limit={take}&sysparm_offset={skip}");
            return json;
        }
        [HttpGet("ServiceManagement/SlaExceeded")]
        public async Task<ActionResult<string>> GetSlaExceeded(int skip = 0, int take = 100)
        {
            var json = await _oboIdentityApi.CallWebApiForUserAsync<MaintenanceAgreementEndDate>(Apis.Snow, $"{Service.SlaExceeded}&sysparm_limit={take}&sysparm_offset={skip}");
            return json;
        }
        [HttpGet("ServiceManagement/SlaMet")]
        public async Task<ActionResult<string>> GetSlaMet(int skip = 0, int take = 100)
        {
            var json = await _oboIdentityApi.CallWebApiForUserAsync<MaintenanceAgreementEndDate>(Apis.Snow, $"{Service.SlaMet}&sysparm_limit={take}&sysparm_offset={skip}");
            return json;
        }
        [HttpGet("ServiceManagement/SlaMetList")]
        public async Task<ActionResult<string>> GetSlaMetList(int skip = 0, int take = 100)
        {
            var json = await _oboIdentityApi.CallWebApiForUserAsync<MaintenanceAgreementEndDate>(Apis.Snow, $"{Service.SlaMetList}&sysparm_limit={take}&sysparm_offset={skip}");
            return json;
        }
        [HttpGet("ServiceManagement/SlaNotMetList")]
        public async Task<ActionResult<string>> GetSlaNotMetList(int skip = 0, int take = 100)
        {
            var json = await _oboIdentityApi.CallWebApiForUserAsync<MaintenanceAgreementEndDate>(Apis.Snow, $"{Service.SlaNotMetList}&sysparm_limit={take}&sysparm_offset={skip}");
            return json;
        }

        #endregion


        #region Software Inventory Management


        [HttpGet("ServiceManagement/SoftwareInventory/Allsoftware")]
        public async Task<ActionResult<string>> AllSoftware(int skip = 0, int take = 100)
        {
            var json = await _oboIdentityApi.CallWebApiForUserAsync<MaintenanceAgreementEndDate>(Apis.Snow, $"{SoftwareInventory.AllSoftware}&sysparm_limit={take}&sysparm_offset={skip}");
            return json;
        }
        [HttpGet("ServiceManagement/ServiceManagement/SoftwareInventory/SoftwareWithStatusPie")]
        public async Task<ActionResult<string>> GetSoftwareWithStatusPie(int skip = 0, int take = 100)
        {
            var json = await _oboIdentityApi.CallWebApiForUserAsync<MaintenanceAgreementEndDate>(Apis.Snow, $"{SoftwareInventory.SoftwareWithStatusPie}&sysparm_limit={take}&sysparm_offset={skip}");
            return json;
        }
        
        #endregion
    }
}