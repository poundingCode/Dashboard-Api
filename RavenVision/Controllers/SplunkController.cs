﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Client;
using Microsoft.Identity.Web;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using static RavenVision.Models.Constants;

namespace RavenVision.Controllers.Graph
{
    // [Authorize]
    // [RequiredScope("access_as_user")]
    [Route("api/[controller]")]
    [ApiController]
    public class SplunkController : ControllerBase
    {
        private readonly string[] scopeRequiredByApi;
        private readonly ITokenAcquisition _tokenAcquisition;

        private IDownstreamWebApi _downstreamWebApi;

        private readonly IConfiguration _configuration;
        /// <summary>
        /// The Web API will only accept tokens 
        /// 1) for users, and 
        ///  2) having the access_as_user scope for this API
        /// https://login.microsoftonline.us/da8e06a2-ecd4-4f83-a1c3-cdf8f2f75525/oauth2/v2.0/token
        /// https://dccodev-cluster.usgovvirginia.cloudapp.usgovcloudapi.net:8000/user_impersonation
        /// </summary>
        /// <param name="tokenAcquisition"></param>
        /// <param name="dataSource"></param>
        public SplunkController(ITokenAcquisition tokenAcquisition,  IDownstreamWebApi downstreamWebApi, IConfiguration iconfig)
        {
            _tokenAcquisition = tokenAcquisition;
            _configuration = iconfig;
            var uri = _configuration.GetValue<string>($"Scopes:Splunk:BaseUrl");
            scopeRequiredByApi = new string[] { uri + ".default" };// "user_impersonation", "access_as_user" };
           
            _downstreamWebApi = downstreamWebApi;
        }


        [HttpGet("Details")]
        public async Task<ActionResult> Details()
        {
            try
            {
                var value = await _downstreamWebApi.CallWebApiForUserAsync(
               Apis.Splunk,
               options =>
               {
                   options.RelativePath = $"me";
               });
                return Ok(value);
            }
            catch (MicrosoftIdentityWebChallengeUserException ex)
            {
                _tokenAcquisition.ReplyForbiddenWithWwwAuthenticateHeader(scopeRequiredByApi, ex.MsalUiRequiredException);
                return Ok(string.Empty);
            }
            catch (MsalUiRequiredException ex)
            {
                _tokenAcquisition.ReplyForbiddenWithWwwAuthenticateHeader(scopeRequiredByApi, ex);
                return Ok(string.Empty);
            }
            catch (MsalException ex)
            {
                HttpContext.Response.ContentType = "application/json";
                HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("An authentication error occurred while acquiring a token for downstream API\n" + ex.ErrorCode + "\n" + ex.Message));
            }
            catch (Exception ex)
            {
                if (ex.InnerException is MicrosoftIdentityWebChallengeUserException challengeException)
                {
                    //  _tokenAcquisition.ReplyForbiddenWithWwwAuthenticateHeader(_graphOptions.Value.Scopes.Split(' '), challengeException.MsalUiRequiredException);
                    HttpContext.Response.ContentType = "application/json";
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("interaction required"));
                }
                else
                {
                    HttpContext.Response.ContentType = "application/json";
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("An error occurred while calling the downstream API\n" + ex.Message));
                }
            }
            return null;
        }
         
        //[HttpGet("SearchJobs")]
        //[AuthorizeForScopes(Scopes = new[] { Scopes.UserRead })]
        //public async Task<ActionResult<ICollection<Dictionary<string, object>>>> GetSearchJobs()
        //{
        //    //  HttpContext.VerifyUserHasAnyAcceptedScope(scopeRequiredByApi);
        //    try
        //    {
        //        var list = _dataSource.searchJobs(CancellationToken.None);
        //        return Ok(list);
        //    }
        //    catch (MsalException ex)
        //    {
        //        HttpContext.Response.ContentType = "application/json";
        //        HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
        //        await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("An authentication error occurred while acquiring a token for downstream API\n" + ex.ErrorCode + "\n" + ex.Message));
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.InnerException is MicrosoftIdentityWebChallengeUserException challengeException)
        //        {
        //          //  _tokenAcquisition.ReplyForbiddenWithWwwAuthenticateHeader(_graphOptions.Value.Scopes.Split(' '), challengeException.MsalUiRequiredException);
        //            HttpContext.Response.ContentType = "application/json";
        //            HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
        //            await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("interaction required"));
        //        }
        //        else
        //        {
        //            HttpContext.Response.ContentType = "application/json";
        //            HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
        //            await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("An error occurred while calling the downstream API\n" + ex.Message));
        //        }
        //    }
        //    return null;
        //}
    }
}
