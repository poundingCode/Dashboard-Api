﻿
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace RavenVision.DataSources.API
{
    public class ApiBuilder
    {
        private readonly IConfiguration _configuration;
        private readonly string _area;

        public ApiBuilder(IConfiguration iconfig, string area)
        {
            _configuration = iconfig;
            _area = area;
        }

        public async Task<ICollection<Dictionary<string, object>>> GetApi(string relativePath, CancellationToken cancellationToken)
        {
            var api = new DataLakeSource<Dictionary<string, object>>(_configuration, $"{_area}{relativePath}");
            return await api.GetApi(cancellationToken);
        }

        public async Task<ICollection<Dictionary<string, object>>> PostApi(string relativePath, CancellationToken cancellationToken)
        {
            var api = new DataLakeSource<Dictionary<string, object>>(_configuration, $"{_area}{relativePath}");
            return await api.PostApi(cancellationToken);
        }

        public async Task<Dictionary<string, object>> GetApiItem(string relativePath, CancellationToken cancellationToken)
        {
            var api = new DataLakeSource<Dictionary<string, object>>(_configuration, $"{_area}{relativePath}");
            return await api.GetApiItem(cancellationToken);
        }

        public async Task<ICollection<Dictionary<string, object>>> GetApi(string relativePath, string key, string value, CancellationToken cancellationToken)
        {
            var api = new DataLakeSource<Dictionary<string, object>>(_configuration, $"{_area}{relativePath}/{value}");
            Dictionary<string, string> list = new()
            {
                { key, value }
            };
            return await api.GetApi(list, cancellationToken);
        }
     
        public async Task<string> CSV(string path, CancellationToken cancellationToken)
        {
            var api = new DataLakeSource<List<string>>(_configuration, $"{_area}{path}");
            var response = await api.GetHttpResponse(cancellationToken);
            return await response.Content.ReadAsStringAsync(cancellationToken);
        }

        public async Task<string> XML(string path, CancellationToken cancellationToken)
        {
            var api = new DataLakeSource<Dictionary<string, object>>(_configuration, $"{ _area }{path}");
            var response = await api.GetHttpResponse(cancellationToken);
            return await response.Content.ReadAsStringAsync(cancellationToken);
        }
    }
}
