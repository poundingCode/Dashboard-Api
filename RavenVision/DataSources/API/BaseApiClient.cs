﻿using RavenVision.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace RavenVision.DataSources.API
{
    /// <summary>
    /// Base API Client class to serve API comms
    /// </summary>
    internal class BaseApiClient<T> where T : class
    {
        private readonly HttpClient httpClient;
        private readonly string apiUrl;
        private readonly Lazy<Newtonsoft.Json.JsonSerializerSettings> _settings;

        public BaseApiClient(HttpClient httpClient, string apiUrl, Dictionary<string, string> headers)
        {
            this.httpClient = httpClient;
            this.apiUrl = apiUrl;
            Headers = headers;
            _settings = new Lazy<Newtonsoft.Json.JsonSerializerSettings>(CreateSerializerSettings);
        }

        private Newtonsoft.Json.JsonSerializerSettings CreateSerializerSettings()
        {
            var settings = new Newtonsoft.Json.JsonSerializerSettings();
            UpdateJsonSerializerSettings(settings);
            return settings;
        }

        /// <summary>
        /// Headers for the API Request
        /// </summary>
        public Dictionary<string, string> Headers { get; }

        /// <summary>
        /// 
        /// </summary>
        public bool ReadResponseAsString { get; set; }

        /// <summary>
        /// 
        /// </summary>
        protected Newtonsoft.Json.JsonSerializerSettings JsonSerializerSettings { get { return _settings.Value; } }


        /// <summary>
        /// Invoke the API Call and return the specified type
        /// </summary>
        /// <returns></returns>
        public async Task<T> InvokeApi(string verb, CancellationToken cancellationToken)
        {
            //try
            //{
                var response = await GetHttpResponseMessage(verb, cancellationToken);
                var headers = Enumerable.ToDictionary(response.Headers, h => h.Key, h => h.Value);
                var status = (int)response.StatusCode;
                if (status == 200)
                {
                    var objectResponse = await ReadObjectResponseAsync<T>(response, headers, cancellationToken).ConfigureAwait(false);
                    if (objectResponse.Object == null)
                    {
                        throw new ApiException("Response was null which was not expected.", status, objectResponse.Text, headers, null);
                    }
                    return objectResponse.Object;
                }
                else
                {
                    var responseData_ = response.Content == null ? null : await response.Content.ReadAsStringAsync(cancellationToken).ConfigureAwait(false);
                    throw new ApiException("The HTTP status code of the response was not expected (" + status + ").", status, responseData_, headers, null);
                }
        //}
        //    catch (Exception e)
        //    {
        //        // yes this is an antipattern. but good for setting break points dev at catching casting issues. ;-) 
        //         throw new Exception($"The HTTP status code of the response was not expected. {e.Message}");
        //    }
}
        
        /// <summary>
        /// Invoke the API Call and return the specified type
        /// </summary>
        /// <returns></returns>
        public async Task<HttpResponseMessage> GetHttpResponseMessage(string verb, CancellationToken cancellationToken)
        {
            using var request = new HttpRequestMessage();
            HttpResponseMessage response = null;
            
            request.Method = new HttpMethod(verb);
            request.Headers.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/json"));
            if(verb == "POST")
            {
                request.Headers.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/problem+json charset=utf-8"));
            }
            
            try
            {
                foreach (var header in Headers)
                {
                    // request.Headers.Add(header.Key, header.Value);
                    request.Headers.TryAddWithoutValidation(header.Key, header.Value);
                }

                PrepareRequest(httpClient, request, apiUrl);

                request.RequestUri = new Uri(apiUrl, UriKind.RelativeOrAbsolute);

                response = await httpClient.SendAsync(request, HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                var headers = Enumerable.ToDictionary(response.Headers, h => h.Key, h => h.Value);
                if (response.Content != null && response.Content.Headers != null)
                {
                    foreach (var item_ in response.Content.Headers)
                        headers[item_.Key] = item_.Value;
                }

                ProcessResponse(httpClient, response);

                return response;
            }
            catch (Exception e)
            {
                // todo remove this before prod
                throw new Exception($"The HTTP status code of the response was not expected. {e.Message}");

            }
        }

        internal virtual void PrepareRequest(HttpClient client, HttpRequestMessage request, string url) { }

        internal virtual void ProcessResponse(HttpClient client, HttpResponseMessage response) { }

        internal virtual void UpdateJsonSerializerSettings(Newtonsoft.Json.JsonSerializerSettings settings) { }

        protected virtual async Task<ObjectResponseResult<U>> ReadObjectResponseAsync<U>(HttpResponseMessage response, IReadOnlyDictionary<string, IEnumerable<string>> headers, CancellationToken cancellationToken)
        {
            if (response == null || response.Content == null)
            {
                return new ObjectResponseResult<U>(default(U), string.Empty);
            }

            if (ReadResponseAsString)
            {
                var responseText = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                try
                {
                    var typedBody = Newtonsoft.Json.JsonConvert.DeserializeObject<U>(responseText, JsonSerializerSettings);
                    return new ObjectResponseResult<U>(typedBody, responseText);
                }
                catch (Newtonsoft.Json.JsonException exception)
                {
                    var message = "Could not deserialize the response body string as " + typeof(T).FullName + ".";
                    throw new ApiException(message, (int)response.StatusCode, responseText, headers, exception);
                }
            }
            else
            {
                try
                {
                    using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    using (var streamReader = new StreamReader(responseStream))
                    using (var jsonTextReader = new Newtonsoft.Json.JsonTextReader(streamReader))
                    {
                        var serializer = Newtonsoft.Json.JsonSerializer.Create(JsonSerializerSettings);
                        var typedBody = serializer.Deserialize<U>(jsonTextReader);
                        return new ObjectResponseResult<U>(typedBody, string.Empty);
                    }
                }
                catch (Newtonsoft.Json.JsonException exception)
                {
                    var message = "Could not deserialize the response body stream as " + typeof(T).FullName + ".";
                    throw new ApiException(message, (int)response.StatusCode, string.Empty, headers, exception);
                }
            }
        }
        protected struct ObjectResponseResult<U>
        {
            public ObjectResponseResult(U responseObject, string responseText)
            {
                this.Object = responseObject;
                this.Text = responseText;
            }

            public U Object { get; }

            public string Text { get; }
        }
    }
}
