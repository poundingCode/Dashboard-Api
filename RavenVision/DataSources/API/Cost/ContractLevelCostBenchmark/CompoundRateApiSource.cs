﻿using System;

using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RavenVision.Interfaces;


using DCCO.ApiDataObjects.Data.Cost.ContractLevelCostBenchmark;

namespace RavenVision.DataSources.API.Cost.ContractLevelCostBenchmark
{
    public class CompoundRateApiSource : IDataSource<CLCBM_Cagr_Data>
    {
        private readonly IConfiguration _configuration;
        private readonly string DATALAKEAPIURL;
        private const string ENDPOINT = "/Cost/TotalContractCAGR";
        private readonly ILogger<CompoundRateApiSource> _logger;


        public CompoundRateApiSource(IConfiguration iconfig, ILogger<CompoundRateApiSource> logger)
        {
            _configuration = iconfig;
            DATALAKEAPIURL = _configuration.GetValue<string>("Apis:Datalake");
            _logger = logger;
        }

        public List<CLCBM_Cagr_Data> Data { get; internal set; }

        public void Dispose()
        {
            Data = null;
        }

        public async Task GetData(List<KeyValuePair<string, string>> parameters = null)
        {
            try
            {
                using HttpClient client = new();
                var apiCaller = new BaseApiClient<ICollection<CLCBM_Cagr_Data>>(client, DATALAKEAPIURL.TrimEnd('/') + ENDPOINT, new Dictionary<string, string>());
                var apiData = await apiCaller.InvokeApi(CancellationToken.None);
                Data = new List<CLCBM_Cagr_Data>(apiData);
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
            }
        }
    }
}
