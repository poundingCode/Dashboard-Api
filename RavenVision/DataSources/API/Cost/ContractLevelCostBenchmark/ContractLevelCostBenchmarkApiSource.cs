﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RavenVision.Interfaces;


using DCCO.ApiDataObjects.Data.Cost.ContractLevelCostBenchmark;


namespace RavenVision.DataSources.API.Cost.ContractLevelCostBenchmark
{
    public class ContractLevelCostBenchmarkApiSource : IDataSource<CLCBM>
    {
        private readonly IConfiguration _configuration;
        private readonly string DATALAKEAPIURL;
        private readonly ILogger<ContractLevelCostBenchmarkApiSource> _logger;

        private const string ENDPOINT = "/Cost/ContractLevelCostBenchmarks";

        public ContractLevelCostBenchmarkApiSource(IConfiguration iconfig, ILogger<ContractLevelCostBenchmarkApiSource> logger)
        {
            _configuration = iconfig;
            DATALAKEAPIURL = _configuration.GetValue<string>("Apis:Datalake");
            _logger = logger;
        }

        public List<CLCBM> Data { get; internal set; }

        public void Dispose()
        {
            Data = null;
        }

        public async Task GetData(List<KeyValuePair<string, string>> parameters = null)
        {
            try
            {
                using HttpClient client = new();
                var apiCaller = new BaseApiClient<ICollection<CLCBM>>(client, DATALAKEAPIURL.TrimEnd('/') + ENDPOINT, new Dictionary<string, string>());
                var apiData = await apiCaller.InvokeApi(CancellationToken.None);
                // todo ask jerry why this class doesn't have a data property
                Data = new List<CLCBM>(apiData);
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
            }
        }
    }
}
