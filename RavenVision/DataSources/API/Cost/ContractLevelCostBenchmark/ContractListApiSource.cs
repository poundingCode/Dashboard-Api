﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RavenVision.Interfaces;

using DCCO.ApiDataObjects.Data.Cost.BasicList; 

namespace RavenVision.DataSources.API.Cost.ContractLevelCostBenchmark
{
    public class ContractListApiSource : IDataSource<Contract_Data>
    {
        private readonly IConfiguration _configuration;
        private readonly string DATALAKEAPIURL;
        private readonly ILogger<ContractListApiSource> _logger;
        private const string ENDPOINT = "/Cost/GetContractList";

        public ContractListApiSource(IConfiguration iconfig, ILogger<ContractListApiSource> logger)
        {
            _configuration = iconfig;
            DATALAKEAPIURL = _configuration.GetValue<string>("Apis:Datalake");
            _logger = logger;
        }

        public List<Contract_Data> Data { get; internal set; }

        public void Dispose()
        {
            Data = null;
        }

        public async Task GetData(List<KeyValuePair<string, string>> parameters = null)
        {
            try
            {
                using HttpClient client = new();
                var apiCaller = new BaseApiClient<ContractList>(client, DATALAKEAPIURL.TrimEnd('/') + ENDPOINT, new Dictionary<string, string>());
                var apiData = await apiCaller.InvokeApi(CancellationToken.None);
                Data = new List<Contract_Data>(apiData.Data);
            }
            catch(Exception e)
            {
                _logger.LogError(e.ToString());
            }
}
    }
}
