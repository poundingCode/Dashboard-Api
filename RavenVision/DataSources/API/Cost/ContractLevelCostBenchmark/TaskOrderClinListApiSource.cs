﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RavenVision.Interfaces;

using DCCO.ApiDataObjects.Data.Cost.BasicList;

namespace RavenVision.DataSources.API.Cost.ContractLevelCostBenchmark
{
    public class TaskOrderClinListApiSource : IDataSource<TaskOrderClin_Data>
    {
        private readonly IConfiguration _configuration;
        private readonly string DATALAKEAPIURL;
        private readonly ILogger<TaskOrderClinListApiSource> _logger;
        private const string ENDPOINT = "/Cost/GetTaskOrderClinList";

        public TaskOrderClinListApiSource(IConfiguration iconfig, ILogger<TaskOrderClinListApiSource> logger)
        {
            _configuration = iconfig;
            DATALAKEAPIURL = _configuration.GetValue<string>("Apis:Datalake");
            _logger = logger;
        }

        public List<TaskOrderClin_Data> Data { get; internal set; }

        public void Dispose()
        {
            Data = null;
        }

        public async Task GetData(List<KeyValuePair<string, string>> parameters = null)
        {
            try
            {
                using HttpClient client = new();
                var apiCaller = new BaseApiClient<TaskOrderClinList>(client, DATALAKEAPIURL.TrimEnd('/') + ENDPOINT, new Dictionary<string, string>());
                var apiData = await apiCaller.InvokeApi(CancellationToken.None);
                Data = new List<TaskOrderClin_Data>(apiData.Data);
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
            }
        }
    }
}
