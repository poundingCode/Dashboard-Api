﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RavenVision.Interfaces;

using DCCO.ApiDataObjects.Data.Cost.ContractLevelCostBenchmark;

namespace RavenVision.DataSources.API.Cost.ContractLevelCostBenchmark
{
    /// <summary>
    /// API Source for Total Contract Expenditure data
    /// </summary>
    public class TotalContractExpenditureApiSource : IDataSource<CLCBM_Expenditure_Data>
    {
        private readonly IConfiguration _configuration;
        private readonly string DATALAKEAPIURL;
        private readonly ILogger<TotalContractExpenditureApiSource> _logger;
        private const string ENDPOINT = "/Cost/TotalContractExpenditures";
        public TotalContractExpenditureApiSource(IConfiguration iconfig, ILogger<TotalContractExpenditureApiSource> logger)
        {
            _configuration = iconfig;
            DATALAKEAPIURL = _configuration.GetValue<string>("Apis:Datalake");
            _logger = logger;
        }

        /// <summary>
        /// Interface required preoprty for returning Data
        /// </summary>
        public List<CLCBM_Expenditure_Data> Data { get; internal set; }

        public void Dispose()
        {
            Data = null;
        }

        /// <summary>
        /// Interface required method for getting Data
        /// </summary>
        /// <param name="parameters"></param>
        public async Task GetData(List<KeyValuePair<string, string>> parameters = null)
        {
            try
            {
                using HttpClient client = new();
                var apiCaller = new BaseApiClient<ICollection<CLCBM_Expenditure_Data>>(client, DATALAKEAPIURL.TrimEnd('/') + ENDPOINT, new Dictionary<string, string>());
                var apiData = await apiCaller.InvokeApi(CancellationToken.None);
                Data = new List<CLCBM_Expenditure_Data>(apiData);
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
            }
        }
    }
}
