﻿using RavenVision.Interfaces;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
 
using DCCO.ApiDataObjects.Data.Cost.ContractLevelCostSummary;

namespace RavenVision.DataSources.API.Cost.ContractLevelCostSummary
{
    public class ByTaskOrderApiSource : IDataSource<CLCS_ByTaskOrder_Data>
    {
        private readonly IConfiguration _configuration;
        private readonly string DATALAKEAPIURL;
        private const string ENDPOINT = "/Cost/TotalPurchasingByTaskOrder";

        public ByTaskOrderApiSource(IConfiguration iconfig)
        {
            _configuration = iconfig;
            DATALAKEAPIURL = _configuration.GetValue<string>("Apis:Datalake");
        }

        public List<CLCS_ByTaskOrder_Data> Data { get; internal set; }

        public void Dispose()
        {
            Data = null;
        }

        public async Task GetData(List<KeyValuePair<string, string>> parameters = null)
        {
            using HttpClient client = new();
            var apiCaller = new BaseApiClient<CLCS_ByTaskOrder>(client, DATALAKEAPIURL.TrimEnd('/') + ENDPOINT, new Dictionary<string, string>());
            var apiData = await apiCaller.InvokeApi(CancellationToken.None);
            Data = new List<CLCS_ByTaskOrder_Data>(apiData.Data);
        }
    }
}
