﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using static RavenVision.Models.Constants;

namespace RavenVision.DataSources.API
{
    public class DataLakeSource<T> : IDataLakeSource<T>  where T : class
    {
        private readonly IConfiguration _configuration;
        private readonly string DATALAKEAPIURL;
        private readonly string endpoint;

        public DataLakeSource(IConfiguration iconfig,string endpoint, string url = $"Scopes:DataLake:BaseUrl" )
        {
            _configuration = iconfig;
            // https://datalake-api-dev.azurewebsites.us/api/
            DATALAKEAPIURL = _configuration.GetValue<string>(url);
            this.endpoint = endpoint;
        }

        public async Task<ICollection<T>> GetApi(Dictionary<string, string> headers, CancellationToken cancellationToken, string verb = "GET")
        {
            HttpClientHandler handler = new()
            {
                ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
            };
            using HttpClient client = new(handler);

            var apiCaller = new BaseApiClient<IEnumerable< T >> (client, DATALAKEAPIURL + endpoint, headers);
            var apiData = await apiCaller.InvokeApi(verb, cancellationToken);
            return new List<T>(apiData);
        }

        public async Task<ICollection<T>> GetApi(CancellationToken cancellationToken)
        {
            return await  GetApi(new Dictionary<string, string>(), cancellationToken);
        }

        public async Task<ICollection<T>> PostApi(CancellationToken cancellationToken)
        {
            return await GetApi(new Dictionary<string, string>(), cancellationToken, "POST");
        }

        public async Task<T> GetApiItem(CancellationToken cancellationToken)
        {
            using HttpClient client = new();
            var apiCaller = new BaseApiClient<T>(client, DATALAKEAPIURL + endpoint, new Dictionary<string, string>());
            var apiData = await apiCaller.InvokeApi("GET", cancellationToken);
            return apiData;
        }

        public async Task<HttpResponseMessage> GetHttpResponse(CancellationToken cancellationToken)
        {
            using HttpClient client = new();
            var apiCaller = new BaseApiClient<T>(client, DATALAKEAPIURL + endpoint, new Dictionary<string, string>());
            var apiData = await apiCaller.GetHttpResponseMessage("GET", cancellationToken);
            return apiData;
        }
    }
}
