﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace RavenVision.DataSources.API
{
    /// <summary>
    /// Interface for Total Contract Expenditure Data
    /// </summary>
    public interface IDataLakeSource<T> where T : class
    {
        /// <summary>
        /// Get the data with the specified parameters
        /// </summary>
        /// <param name="parameters">List of KeyValuePairs for specifying any filters on the data.</param>
        public Task<ICollection<T>> GetApi(Dictionary<string, string> parameters, CancellationToken cancellationToken, string verb = "GET");

        public Task<ICollection<T>> GetApi(CancellationToken cancellationToken);


        public Task<HttpResponseMessage> GetHttpResponse(CancellationToken cancellationToken);

        public Task<T> GetApiItem(CancellationToken cancellationToken);

        public Task<ICollection<T>> PostApi(CancellationToken cancellationToken);
    }
}
