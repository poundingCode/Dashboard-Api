﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Web;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace RavenVision.DataSources.API
{
    public interface IOboIdentityApi
    {
        Task<ActionResult<string>> CallWebApiForUserAsync<T>(string serviceName, string relativePath, System.Security.Claims.ClaimsPrincipal user = null, StringContent content = null);
    }
}
