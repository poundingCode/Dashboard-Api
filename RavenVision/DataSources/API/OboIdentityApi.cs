﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Client;
using Microsoft.Identity.Web;
using Newtonsoft.Json;
using RavenVision.DataSources.API.Snow;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System.Linq;
using System.Security.Claims;

namespace RavenVision.DataSources.API
{
    public class OboIdentityApi : ControllerBase, IOboIdentityApi
    {
        private readonly ITokenAcquisition _tokenAcquisition;

        private IDownstreamWebApi _downstreamWebApi;

        private List<string> _scopesToAccessApi;

        private readonly IConfiguration _configuration;
        /// <summary>
        /// The Web API will only accept tokens 
        /// 1) for users, and 
        ///  2) having the access_as_user scope for this API
        /// https://login.microsoftonline.us/da8e06a2-ecd4-4f83-a1c3-cdf8f2f75525/oauth2/v2.0/token
        /// https://dccodev-cluster.usgovvirginia.cloudapp.usgovcloudapi.net:8000/user_impersonation
        /// </summary>
        /// <param name="tokenAcquisition"></param>
        /// <param name="dataSource"></param>
        public OboIdentityApi(ITokenAcquisition tokenAcquisition, IDownstreamWebApi downstreamWebApi, IConfiguration iconfig)
        {
            _tokenAcquisition = tokenAcquisition;
            _configuration = iconfig;
            _downstreamWebApi = downstreamWebApi;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceName"></param>
        /// <param name="apiOptions"></param>
        /// <param name="user"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public async Task<ActionResult<string>> CallWebApiForUserAsync<T>(string serviceName, string relativePath, System.Security.Claims.ClaimsPrincipal user = null, StringContent content = null)
        {
            try
            {
                _scopesToAccessApi = new()
                {
                    _configuration[$"Scopes:{serviceName}:Roles"]
                };


                //var response = await _downstreamWebApi.CallWebApiForAppAsync(
                //serviceName, null,
                //options => {
                //    options.BaseUrl = _configuration[$"Scopes:{serviceName}:BaseUrl"];
                //   // options.HttpMethod = HttpMethod.Get;
                //    options.RelativePath = relativePath;
                //    options.Scopes = _scopesToAccessApi.First();
                //    options.Tenant = _configuration["AzureAd:TenantId"];
                //},
                //content);

                //var name = _configuration[$"Scopes:{serviceName}:identity:name"];
                //var email = _configuration[$"Scopes:{serviceName}:identity:email"];
                //var claims = new List<Claim>
                //{
                //    new Claim(ClaimTypes.Name, name),
                //    new Claim(ClaimTypes.Email, email),
                //    new Claim("preferred_username", email)
                //};
                //// claims.Add(new Claim(ClaimTypes.Sid, ));
                //var identity = new List<ClaimsIdentity>
                //{
                //    new ClaimsIdentity(claims, "name")
                //};
                //ClaimsPrincipal claimsPrincipal = new(identity);

                var response = await _downstreamWebApi.CallWebApiForUserAsync(
                serviceName,
                options =>
                {
                    options.Scopes = _scopesToAccessApi.First();
                    options.RelativePath = relativePath;
                },
                user,
                content);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var apiResult = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    if (apiResult == "{\"result\":[]}")
                    {
                        return NoContent();
                    }

                    var responselist = new List<T>();
                    
                    var json = JsonConvert.DeserializeObject<Dictionary<string, object>>(apiResult);
                    JsonConvert.PopulateObject(json.Values.FirstOrDefault().ToString(),  responselist);

                    var jsonString = JsonConvert.SerializeObject(responselist, Formatting.Indented);
                    return Ok(responselist);
                 
                }
                if(response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    var error = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    return Unauthorized($"Unauthorized. You do not have access to the backing data source: {response.StatusCode}: {error}");
                }
                else
                {
                    var error = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    return BadRequest($"Invalid status code in the HttpResponseMessage: {response.StatusCode}: {error}");
                }
            }
            catch (MicrosoftIdentityWebChallengeUserException ex)
            {
                _tokenAcquisition.ReplyForbiddenWithWwwAuthenticateHeader(_scopesToAccessApi, ex.MsalUiRequiredException );
                return Unauthorized(string.Empty);
            }
            catch (MsalUiRequiredException ex)
            {
                _tokenAcquisition.ReplyForbiddenWithWwwAuthenticateHeader(_scopesToAccessApi, ex);
                return Forbid(string.Empty);
            }
            catch (MsalException ex)
            {
                HttpContext.Response.ContentType = "application/json";
                HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                var response = HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("An authentication error occurred while acquiring a token for downstream API\n" + ex.ErrorCode + "\n" + ex.Message));
                return Unauthorized(response);
            }
            catch (Exception ex)
            {
                if (ex.InnerException is MicrosoftIdentityWebChallengeUserException challengeException)
                {
                    HttpContext.Response.ContentType = "application/json";
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    var response = HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("interaction required"));
                    return BadRequest(response);

                }
                else
                {
                    HttpContext.Response.ContentType = "application/json";
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    await HttpContext.Response.WriteAsync(JsonConvert.SerializeObject("An error occurred while calling the downstream API\n" + ex.Message));
                    return Problem("An error occurred while calling the downstream API\n" + ex.Message);
                }
            }
        }
    }
}
