﻿
namespace RavenVision.DataSources.API.Snow
{
    public static class HardwareInventory
    {
        public const string DayAgingByHardwareAsset = "api/now/table/alm_asset";

        public const string ExpiringMaintenanceContracts = "api/now/table/ast_contract?sysparm_query=state%3Dactive%5EendsRELATIVELT%40dayofweek%40ahead%4090%5Econtract_model%3Da581e836c3102000b959fd251eba8fba";

        public const string ExpiringMaintenanceContractsByVendor = "api/now/table/ast_contract?sysparm_query=state%3Dactive%5EendsRELATIVELT%40dayofweek%40ahead%4090%5Econtract_model%3Da581e836c3102000b959fd251eba8fba";

        public const string HardwareAssets = "api/now/table/alm_hardware";

        public const string HardwareAssetsByLocation =  "api/now/table/alm_hardware";

        public const string HardwareAssetsByStateAndType = "api/now/table/alm_hardware";

        public const string MaintenanceAgreementEndDates = "api/now/table/ast_contract?sysparm_query=contract_model%3Da581e836c3102000b959fd251eba8fba";
    }

    public static class Service
    {
        public const string Closed = "api/now/table/incident?sysparm_query=active%3Dfalse%5Estate!%3D8%5Esys_created_onONThis%20month%40javascript%3Ags.beginningOfThisMonth()%40javascript%3Ags.endOfThisMonth()";

        public const string Escalated = "api/now/table/incident?sysparm_query=u_escalation%3Dtrue%5Estate!%3D8%5Esys_created_onONThis%20month%40javascript%3Ags.beginningOfThisMonth()%40javascript%3Ags.endOfThisMonth()";

        public const string Initiated = "api/now/table/incident?sysparm_query=state!%3D8%5Esys_created_onONThis%20month%40javascript%3Ags.beginningOfThisMonth()%40javascript%3Ags.endOfThisMonth()";

        public const string MTTR = "api/now/table/incident?sysparm_query=state!%3D8%5Eactive%3Dfalse%5Esys_created_onONThis%20month%40javascript%3Ags.beginningOfThisMonth()%40javascript%3Ags.endOfThisMonth()";

        public const string OpenLastMonth = "api/now/table/incident?sysparm_query=sys_created_onONLast%20month%40javascript%3Ags.beginningOfLastMonth()%40javascript%3Ags.endOfLastMonth()%5Estate!%3D8";

        public const string PreviousMonthDaily = "api/now/table/incident?sysparm_query=sys_created_onONLast%20month%40javascript%3Ags.beginningOfLastMonth()%40javascript%3Ags.endOfLastMonth()";

        public const string SlaExceeded = "api/now/table/incident_sla?sysparm_query=inc_active%3Dtrue%5Etaskslatable_has_breached%3Dtrue%5Einc_sys_created_onONThis%20month%40javascript%3Ags.beginningOfThisMonth()%40javascript%3Ags.endOfThisMonth()";
        
        public const string SlaMet = "api/now/table/incident_sla?sysparm_query=taskslatable_has_breached%3Dfalse%5Einc_state!%3D8%5Einc_sys_created_onONThis%20month%40javascript%3Ags.beginningOfThisMonth()%40javascript%3Ags.endOfThisMonth()";
        
        public const string SlaMetList = "api/now/table/incident_sla?sysparm_query=taskslatable_has_breached%3Dfalse%5Einc_state!%3D8%5Einc_sys_created_onONThis%20month%40javascript%3Ags.beginningOfThisMonth()%40javascript%3Ags.endOfThisMonth()";
        
        public const string SlaNotMetList = "api/now/table/incident_sla?sysparm_query=taskslatable_has_breached%3Dtrue%5Einc_active%3Dtrue%5Einc_sys_created_onONThis%20month%40javascript%3Ags.beginningOfThisMonth()%40javascript%3Ags.endOfThisMonth()";
    }

    public static class SoftwareInventory
    {

        public const string AllSoftware = "api/now/table/alm_license?sysparm_query=install_status%3D1";

        //public const string AllSoftwareItems = "api/now/table/alm_license?sysparm_query=install_status%3D1";

        //public const string AllsoftwareItemswithspecificfacilitylocationData = "api/now/table/alm_license?sysparm_query=install_status%3D1";

        //public const string AllsoftwareItemsWithSpecificFacilityLocationData = "api/now/table/alm_license?sysparm_query=install_status%3D1";

        public const string SoftwareWithStatusPie = "api/now/table/cmdb_ci?sysparm_query=category%3DSoftware";
        
        
        //public const string SoftwarewithStatusData = "api/now/table/cmdb_ci?sysparm_query=category%3DSoftware";
        
        //public const string SoftwareitemsincludingStatus = "api/now/table/cmdb_ci?sysparm_query=category%3DSoftware";
        
        //public const string SoftwareItemswithStatus = "api/now/table/cmdb_ci?sysparm_query=category%3DSoftware";
    }
}

