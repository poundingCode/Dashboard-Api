﻿using Microsoft.Extensions.DependencyInjection;
using RavenVision.DataSources.API;

namespace RavenVision.DataSources
{
    public static class ApiBootstrapper
    {
        /// <summary>
        /// Add in the API Data Sources to di
        /// </summary>
        /// <param name="services"></param>
        public static void AddApiDataSources(this IServiceCollection services)
        {
            // use configuration to perform advanced registration patterns
            services.AddTransient<IOboIdentityApi, OboIdentityApi>();
        }
    }
}
