﻿namespace RavenVision.Models
{
    public static class Constants
    {
        public const string BearerAuthorizationScheme = "Bearer";
        public static class Apis
        {
            public const string Datalake = "Datalake";
            public const string Splunk = "Splunk";
            public const string Snow = "Snow";
        }
    }

    public static class Scopes
    {
        public const string UserRead = "User.Read";
        public const string access_as_user = "access_as_user";
        public const string user_impersonation = "user_impersonation";
        public const string TodoList = "TodoList:Scopes";
    }
}
