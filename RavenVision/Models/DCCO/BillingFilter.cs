﻿using System;
using System.Text;

namespace RavenVision.Model.DCCO
{
    public partial class BillingFilter
    {
        #region Data Members
        public int? ContractId { get; set; }
        public int? TaskOrderId { get; set; }
        public int? TaskOrderClinId { get; set; }
        public int? TaskOrderClinCategoryId { get; set; }
        public int? ChargingItemId { get; set; }
        public int? IdiqClinId { get; set; }
        public int? ContractYear { get; set; }
        public int? FlagSubcontrator { get; set; }
        public int? LaborCategoryId { get; set; }
        public int? ComponentId { get; set; }
        public DateTime? InvoiceDate_Start { get; set; }
        public DateTime? InvoiceDate_End { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public int? Quarter { get; set; }
        #endregion

        #region Constructor
        /// <summary> Constructor for FactBillingDetail_Filter class. </summary>
        public BillingFilter()
            : base()
        {
            this.ContractId = null;
            this.TaskOrderId = null;
            this.TaskOrderClinId = null;
            this.TaskOrderClinCategoryId = null;
            this.ChargingItemId = null;
            this.IdiqClinId = null;
            this.ContractYear = null;
            this.FlagSubcontrator = null;
            this.LaborCategoryId = null;
            this.ComponentId = null;
            this.InvoiceDate_Start = null;
            this.InvoiceDate_End = null;
            this.Year = null;
            this.Month = null;
            this.Quarter = null;
        }
        #endregion

        #region ToString
        /// <summary>
        /// This method writes out the filter values that are being applied.  Useful for error messages...
        /// </summary>
        public override string ToString()
        {

            // initialize the string builder
            StringBuilder sb = new();
            sb.Append("FACT_BILLING_DETAIL filter:" + Environment.NewLine);

            // add the ContractIdvalue -- [INT]
            sb.Append("  ContractId: ");
            if (this.ContractId.HasValue)
            {
                sb.Append(this.ContractId);
            }
            else
            {
                sb.Append("null");
            }
            sb.Append(Environment.NewLine);

            // add the TaskOrderIdvalue -- [INT]
            sb.Append("  TaskOrderId: ");
            if (this.TaskOrderId.HasValue)
            {
                sb.Append(this.TaskOrderId);
            }
            else
            {
                sb.Append("null");
            }
            sb.Append(Environment.NewLine);

            // add the TaskOrderClinIdvalue -- [INT]
            sb.Append("  TaskOrderClinId: ");
            if (this.TaskOrderClinId.HasValue)
            {
                sb.Append(this.TaskOrderClinId);
            }
            else
            {
                sb.Append("null");
            }
            sb.Append(Environment.NewLine);

            // add the TaskOrderClinCategoryIdvalue -- [INT]
            sb.Append("  TaskOrderClinCategoryId: ");
            if (this.TaskOrderClinCategoryId.HasValue)
            {
                sb.Append(this.TaskOrderClinCategoryId);
            }
            else
            {
                sb.Append("null");
            }
            sb.Append(Environment.NewLine);

            // add the ChargingItemIdvalue -- [INT]
            sb.Append("  ChargingItemId: ");
            if (this.ChargingItemId.HasValue)
            {
                sb.Append(this.ChargingItemId);
            }
            else
            {
                sb.Append("null");
            }
            sb.Append(Environment.NewLine);

            // add the IdiqClinIdvalue -- [INT]
            sb.Append("  IdiqClinId: ");
            if (this.IdiqClinId.HasValue)
            {
                sb.Append(this.IdiqClinId);
            }
            else
            {
                sb.Append("null");
            }
            sb.Append(Environment.NewLine);

            // add the ContractYearvalue -- [INT]
            sb.Append("  ContractYear: ");
            if (this.ContractYear.HasValue)
            {
                sb.Append(this.ContractYear);
            }
            else
            {
                sb.Append("null");
            }
            sb.Append(Environment.NewLine);

            // add the FlagSubcontratorvalue -- [INT]
            sb.Append("  FlagSubcontrator: ");
            if (this.FlagSubcontrator.HasValue)
            {
                sb.Append(this.FlagSubcontrator);
            }
            else
            {
                sb.Append("null");
            }
            sb.Append(Environment.NewLine);

            // add the LaborCategoryIdvalue -- [INT]
            sb.Append("  LaborCategoryId: ");
            if (this.LaborCategoryId.HasValue)
            {
                sb.Append(this.LaborCategoryId);
            }
            else
            {
                sb.Append("null");
            }
            sb.Append(Environment.NewLine);

            // add the ComponentIdvalue -- [INT]
            sb.Append("  ComponentId: ");
            if (this.ComponentId.HasValue)
            {
                sb.Append(this.ComponentId);
            }
            else
            {
                sb.Append("null");
            }
            sb.Append(Environment.NewLine);

            // add the InvoiceDatevalue -- [DATE]
            sb.Append("  InvoiceDate_Start: ");
            if (this.InvoiceDate_Start.HasValue)
            {
                sb.Append(this.InvoiceDate_Start.Value.ToShortDateString() + "  " + this.InvoiceDate_Start.Value.ToShortTimeString());
            }
            else
            {
                sb.Append("null");
            }
            sb.Append(Environment.NewLine);

            sb.Append("  InvoiceDate_End: ");
            if (this.InvoiceDate_End.HasValue)
            {
                sb.Append(this.InvoiceDate_End.Value.ToShortDateString() + "  " + this.InvoiceDate_End.Value.ToShortTimeString());
            }
            else
            {
                sb.Append("null");
            }

            sb.Append(Environment.NewLine);

            // add the Yearvalue -- [INT]
            sb.Append("  Year: ");
            if (this.Year.HasValue)
            {
                sb.Append(this.Year);
            }
            else
            {
                sb.Append("null");
            }
            sb.Append(Environment.NewLine);

            // add the Monthvalue -- [INT]
            sb.Append("  Month: ");
            if (this.Month.HasValue)
            {
                sb.Append(this.Month);
            }
            else
            {
                sb.Append("null");
            }
            sb.Append(Environment.NewLine);

            // add the Quartervalue -- [INT]
            sb.Append("  Quarter: ");
            if (this.Quarter.HasValue)
            {
                sb.Append(this.Quarter);
            }
            else
            {
                sb.Append("null");
            }
            sb.Append(Environment.NewLine);

            // return the filter summary string
            return sb.ToString();

        }
        #endregion

    }

}
