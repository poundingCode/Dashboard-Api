﻿using System;

namespace RavenVision.Model.DCCO
{
    /// <summary>A network metric filter.</summary>
    /// <remarks>Jerry Liverseidge Aa, 7/28/2022.</remarks>
    [Serializable]
    public class NetworkMetricFilter
    {
        /// <summary>Gets or sets the begin stamp.</summary>
        /// <value>The begin stamp.</value>
        public int? BeginStamp { get; set; }

        /// <summary>Gets or sets the end date.</summary>
        /// <value>The end date.</value>
        public DateTime? EndDate { get; set; }

        /// <summary>Gets or sets the end stamp.</summary>
        /// <value>The end stamp.</value>
        public int? EndStamp { get; set; }

        /// <summary>Gets or sets the segment octet 1.</summary>
        /// <value>The segment octet 1.</value>
        public string? SegmentOctet1 { get; set; }

        /// <summary>Gets or sets the segment octet 2.</summary>
        /// <value>The segment octet 2.</value>
        public string? SegmentOctet2 { get; set; }

        /// <summary>Gets or sets the segment octet 3.</summary>
        /// <value>The segment octet 3.</value>
        public string? SegmentOctet3 { get; set; }

        /// <summary>Gets or sets the start date.</summary>
        /// <value>The start date.</value>
        public DateTime? StartDate { get; set; }
    }
}