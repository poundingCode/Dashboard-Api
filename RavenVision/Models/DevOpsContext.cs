﻿using Microsoft.EntityFrameworkCore;

namespace RavenVision.Models
{
    public class DevOpsContext : DbContext
    {
        public DevOpsContext(DbContextOptions<DevOpsContext> options)
            : base(options)
        {

        }
        public DbSet<WorkItem> WorkItems { get; set; }
    }
}
