﻿using Microsoft.Graph;

using Newtonsoft.Json;

namespace RavenVision.Model.Graph.SharePoint
{
    public class DriveVm : BaseItem
    {
        // Summary:
        //     Gets or sets drive type. Describes the type of drive represented by this resource.
        //     OneDrive personal drives will return personal. OneDrive for Business will return
        //     business. SharePoint document libraries will return documentLibrary. Read-only.
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "driveType", Required = Required.Default)]
        public string DriveType
        {
            get;
            set;
        }

        //
        // Summary:
        //     Gets or sets owner. Optional. The user account that owns the drive. Read-only.
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "owner", Required = Required.Default)]
        public IdentitySet Owner
        {
            get;
            set;
        }

        //
        // Summary:
        //     Gets or sets quota. Optional. Information about the drive's storage space quota.
        //     Read-only.
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "quota", Required = Required.Default)]
        public Quota Quota
        {
            get;
            set;
        }

        //
        // Summary:
        //     Gets or sets share point ids.
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "sharePointIds", Required = Required.Default)]
        public SharepointIds SharePointIds
        {
            get;
            set;
        }

        //
        // Summary:
        //     Gets or sets system. If present, indicates that this is a system-managed drive.
        //     Read-only.
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "system", Required = Required.Default)]
        public SystemFacet System
        {
            get;
            set;
        }

        //
        // Summary:
        //     Gets or sets list. For drives in SharePoint, the underlying document library
        //     list. Read-only. Nullable.
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "list", Required = Required.Default)]
        public List List
        {
            get;
            set;
        }

        //
        // Summary:
        //     Gets or sets root. The root folder of the drive. Read-only.
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "root", Required = Required.Default)]
        public DriveItem Root
        {
            get;
            set;
        }
    }
}
