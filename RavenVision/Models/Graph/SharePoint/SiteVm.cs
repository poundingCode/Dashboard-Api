﻿using System;
using System.Collections.Generic;

namespace RavenVision.Model.Graph.SharePoint
{
    public class SiteVm
    {
        public string Id { get; set; }

        public string Description { get; set; }

        public string DisplayName { get; set; }

        public string CreatedByUser { get; set; }

        public DateTimeOffset? CreatedDateTime { get; set; }

        public DateTimeOffset? LastModifiedDateTime { get; set; }

        public List<SiteVm> Items { get; set; }

        public string Name { get; set; }

        public string WebUrl { get; set; }
    }
}
