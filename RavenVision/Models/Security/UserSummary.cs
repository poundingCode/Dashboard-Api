﻿
using Microsoft.Graph;

namespace RavenVision.Model.Security
{
    //
    // Summary:
    //     The type User.
    public class UserSummary //: DirectoryObject
    {
        ////
        //// Summary:
        ////     Gets or sets city. The city in which the user is located. Returned only on $select.
        ////     Supports $filter.
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "city", Required = Required.Default)]
        //public string City
        //{
        //    get;
        //    set;
        //}

        ////
        //// Summary:
        ////     Gets or sets company name. The company name which the user is associated. This
        ////     property can be useful for describing the company that an external user comes
        ////     from. The maximum length of the company name is 64 chararcters.Returned only
        ////     on $select.
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "companyName", Required = Required.Default)]
        //public string CompanyName
        //{
        //    get;
        //    set;
        //}

        ////
        //// Summary:
        ////     Gets or sets country. The country/region in which the user is located; for example,
        ////     'US' or 'UK'. Returned only on $select. Supports $filter.
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "country", Required = Required.Default)]
        //public string Country
        //{
        //    get;
        //    set;
        //}

        ////
        //// Summary:
        ////     Gets or sets created date time. The date and time the user was created. The value
        ////     cannot be modified and is automatically populated when the entity is created.
        ////     The DateTimeOffset type represents date and time information using ISO 8601 format
        ////     and is always in UTC time. Property is nullable. A null value indicates that
        ////     an accurate creation time couldn't be determined for the user. Returned only
        ////     on $select. Read-only. Supports $filter.
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "createdDateTime", Required = Required.Default)]
        //public DateTimeOffset? CreatedDateTime
        //{
        //    get;
        //    set;
        //}

        //
        // Summary:
        //     Gets or sets display name. The name displayed in the address book for the user.
        //     This value is usually the combination of the user's first name, middle initial,
        //     and last name. This property is required when a user is created and it cannot
        //     be cleared during updates. Returned by default. Supports $filter and $orderby.
        public string DisplayName
        {
            get;
            set;
        }

        //
        // Summary:
        //     Gets or sets given name. The given name (first name) of the user. Returned by
        //     default. Supports $filter.
        public string GivenName
        {
            get;
            set;
        }

        //
        // Summary:
        //     Gets or sets id. Read-only.
        public string Id
        {
            get;
            set;
        }

        //
        // Summary:
        //     Gets or sets job title. The user's job title. Returned by default. Supports $filter.
        public string JobTitle
        {
            get;
            set;
        }

        //
        // Summary:
        //     Gets or sets mail. The SMTP address for the user, for example, 'jeff@contoso.onmicrosoft.com'.
        //     Returned by default. Supports $filter and endsWith.
        public string Mail
        {
            get;
            set;
        }


        //
        // Summary:
        //     Gets or sets mobile phone. The primary cellular telephone number for the user.
        //     Returned by default. Read-only for users synced from on-premises directory.
        public string MobilePhone
        {
            get;
            set;
        }


        //
        // Summary:
        //     Gets or sets surname. The user's surname (family name or last name). Returned
        //     by default. Supports $filter.
        public string Surname
        {
            get;
            set;
        }
        //
        // Summary:
        //     Gets or sets manager. The user or contact that is this user's manager. Read-only.
        //     (HTTP Methods: GET, PUT, DELETE.)
        public DirectoryObject Manager
        {
            get;
            set;
        }

        public string ManagerName
        {
            get;
            set;
        }
    }
}
