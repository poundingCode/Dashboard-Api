﻿
using Newtonsoft.Json;

namespace RavenVision.Model.Snow
{ 
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class HardwareAsset
    {
        [JsonProperty(PropertyName = "sys_created_by")]
        public string Sys_created_by { get; set; }

        [JsonProperty(PropertyName = "sys_class_name")]
        public string Sys_class_name { get; set; }

        [JsonProperty(PropertyName = "serial_number")]
        public string Serial_number { get; set; }

        [JsonProperty(PropertyName = "sys_created_on")]
        public string Sys_created_on { get; set; }

        [JsonProperty(PropertyName = "sys_domain")]
        public SnowLink Sys_domain { get; set; }

        [JsonProperty(PropertyName = "display_name")]
        public string Display_name { get; set; }

        [JsonProperty(PropertyName = "sys_id")]
        public string Sys_id { get; set; }

        [JsonProperty(PropertyName = "quantity")]
        public string Quantity { get; set; }

        [JsonProperty(PropertyName = "model_category")]
        public SnowLink Model_category { get; set; }

        [JsonProperty(PropertyName = "model")]
        public SnowLink Model { get; set; }

        [JsonProperty(PropertyName = "ci")]
        public SnowLink Ci { get; set; }

        [JsonProperty(PropertyName = "location")]
        public SnowLink Location { get; set; }

        [JsonProperty(PropertyName = "link")]
        public SnowLink Link { get; set; }
    }
}