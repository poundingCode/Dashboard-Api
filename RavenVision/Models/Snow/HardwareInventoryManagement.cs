﻿
using Newtonsoft.Json;

namespace RavenVision.Model.Snow
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class HardwareInventoryManagement
    {
        [JsonProperty(PropertyName = "contract_model")]
        public SnowLink Contract_model { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "expiration")]
        public SnowLink Expiration { get; set; }

        [JsonProperty(PropertyName = "starts")]
        public string Starts { get; set; }

        [JsonProperty(PropertyName = "sys_created_on")]
        public string Sys_created_on { get; set; }

        [JsonProperty(PropertyName = "sys_domain")]
        public SnowLink Sys_domain { get; set; }

        [JsonProperty(PropertyName = "sys_id")]
        public string Sys_id { get; set; }

        [JsonProperty(PropertyName = "total_cost")]
        public string Total_cost { get; set; }

        [JsonProperty(PropertyName = "vendor")]
        public SnowLink Vendor { get; set; }
    }
}
