﻿
using Newtonsoft.Json;

namespace RavenVision.Model.Snow
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class MaintenanceAgreementEndDate
    {

        [JsonProperty(PropertyName = "number")]
        public string number { get; set; }


        [JsonProperty(PropertyName = "short_description")]
        public string short_description { get; set; }


        [JsonProperty(PropertyName = "sys_created_on")]
        public string sys_created_on { get; set; }


        [JsonProperty(PropertyName = "expiration")]
        public SnowLink expiration { get; set; }


        [JsonProperty(PropertyName = "sys_domain")]
        public SnowLink sys_domain { get; set; }


        [JsonProperty(PropertyName = "sys_id")]
        public string sys_id { get; set; }


        [JsonProperty(PropertyName = "vendor")]
        public SnowLink vendor { get; set; }

        [JsonProperty(PropertyName = "vendor_contract")]
        public string vendor_contract { get; set; }
    }
}
