﻿
using Newtonsoft.Json;

namespace RavenVision.Model.Snow
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class SnowLink
    {
        [JsonProperty(PropertyName = "link")]
        public string link { get; set; }

        [JsonProperty(PropertyName = "value")]
        public string value { get; set; }
    }
}
