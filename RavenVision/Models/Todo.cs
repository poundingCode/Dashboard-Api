﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using Microsoft.Graph;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RavenVision.Models
{
    public class TodoSummary
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        //  public string Owner { get; set; }
        public string Description { get; set; }

        //////
        ////// Summary:
        //////     Gets or sets body. The task body that typically contains information about the
        //////     task.
        ////[JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "body", Required = Required.Default)]
        ////public ItemBody Body
        ////{
        ////    get;
        ////    set;
        ////}

        //////
        ////// Summary:
        //////     Gets or sets body last modified date time. The date and time when the task was
        //////     last modified. By default, it is in UTC. You can provide a custom time zone in
        //////     the request header. The property value uses ISO 8601 format and is always in
        //////     UTC time. For example, midnight UTC on Jan 1, 2020 would look like this: '2020-01-01T00:00:00Z'.
        ////[JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "bodyLastModifiedDateTime", Required = Required.Default)]
        ////public DateTimeOffset? BodyLastModifiedDateTime
        ////{
        ////    get;
        ////    set;
        ////}

        ////
        //// Summary:
        ////     Gets or sets completed date time. The date in the specified time zone that the
        ////     task was finished.
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "completedDateTime", Required = Required.Default)]
        //public DateTimeTimeZone CompletedDateTime
        //{
        //    get;
        //    set;
        //}

        ////
        //// Summary:
        ////     Gets or sets created date time. The date and time when the task was created.
        ////     By default, it is in UTC. You can provide a custom time zone in the request header.
        ////     The property value uses ISO 8601 format. For example, midnight UTC on Jan 1,
        ////     2020 would look like this: '2020-01-01T00:00:00Z'.
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "createdDateTime", Required = Required.Default)]
        //public DateTimeOffset? CreatedDateTime
        //{
        //    get;
        //    set;
        //}

        //
        // Summary:
        //     Gets or sets due date time. The date in the specified time zone that the task
        //     is to be finished.
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "dueDateTime", Required = Required.Default)]
        public DateTimeTimeZone DueDateTime
        {
            get;
            set;
        }

        //
        // Summary:
        //     Gets or sets importance. The importance of the task. Possible values are: low,
        //     normal, high.
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "importance", Required = Required.Default)]
        public string Importance
        {
            get;
            set;
        }

        ////
        //// Summary:
        ////     Gets or sets is reminder on. Set to true if an alert is set to remind the user
        ////     of the task.
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "isReminderOn", Required = Required.Default)]
        //public bool? IsReminderOn
        //{
        //    get;
        //    set;
        //}

        //
        // Summary:
        //     Gets or sets last modified date time. The date and time when the task was last
        //     modified. By default, it is in UTC. You can provide a custom time zone in the
        //     request header. The property value uses ISO 8601 format and is always in UTC
        //     time. For example, midnight UTC on Jan 1, 2020 would look like this: '2020-01-01T00:00:00Z'.
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "lastModifiedDateTime", Required = Required.Default)]
        public DateTimeOffset? LastModifiedDateTime
        {
            get;
            set;
        }

        ////
        //// Summary:
        ////     Gets or sets recurrence. The recurrence pattern for the task.
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "recurrence", Required = Required.Default)]
        //public PatternedRecurrence Recurrence
        //{
        //    get;
        //    set;
        //}

        ////
        //// Summary:
        ////     Gets or sets reminder date time. The date and time for a reminder alert of the
        ////     task to occur.
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "reminderDateTime", Required = Required.Default)]
        //public DateTimeTimeZone ReminderDateTime
        //{
        //    get;
        //    set;
        //}

        //
        // Summary:
        //     Gets or sets status. Indicates the state or progress of the task. Possible values
        //     are: notStarted, inProgress, completed, waitingOnOthers, deferred.
        // [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "status", Required = Required.Default)]
        public string Status
        {
            get;
            set;
        }

        ////
        //// Summary:
        ////     Gets or sets title. A brief description of the task.
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "title", Required = Required.Default)]
        //public string Title
        //{
        //    get;
        //    set;
        //}

    }
}