﻿using Microsoft.EntityFrameworkCore;

namespace RavenVision.Models
{
    public class TodoContext : DbContext
    {
        public TodoContext(DbContextOptions<TodoContext> options)
            : base(options)
        {

        }

        public DbSet<TodoSummary> TodoSummaryItems { get; set; }
    }
}
