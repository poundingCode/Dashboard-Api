﻿using System.ComponentModel.DataAnnotations;

namespace RavenVision.Models
{
    public class WorkItem
    {
        [Key]
        public string WorkItemId { get; set; }
        public string WorkItemType { get; set; }
        public string Title { get; set; }
    }
}
