﻿using Microsoft.Graph;
using System.Threading.Tasks;

namespace RavenVision.Services.Contracts
{
    public interface IGraphService
    {
        Task<ISiteListsCollectionPage> GetSiteLists(string site);
    }
}