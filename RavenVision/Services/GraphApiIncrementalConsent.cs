﻿
//using System.Threading.Tasks;
//using Microsoft.Graph;
//using Microsoft.Identity.Client;
//using Microsoft.Identity.Web;

//namespace RavenVision.Services
//{
//    public class GraphApiIncrementalConsent
//    {
//        private readonly ITokenAcquisition _tokenAcquisition;

//        public GraphApiIncrementalConsent(ITokenAcquisition tokenAcquisition)
//        {
//            _tokenAcquisition = tokenAcquisition;
//        }

//        public async Task<string> CallGraphApiOnBehalfOfUser()
//        {
//            string[] scopes = { "user.read" };

//            // we use MSAL.NET to get a token to call the API On Behalf Of the current user
//            try
//            {
//                string accessToken = await _tokenAcquisition.GetAccessTokenForUserAsync(scopes);
//                dynamic me = await CallGraphApiOnBehalfOfUser(accessToken);
//                return me.UserPrincipalName;
//            }
//            catch (MicrosoftIdentityWebChallengeUserException ex)
//            {
//                _tokenAcquisition.ReplyForbiddenWithWwwAuthenticateHeader(scopes, ex.MsalUiRequiredException);
//                return string.Empty;
//            }
//            catch (MsalUiRequiredException ex)
//            {
//                _tokenAcquisition.ReplyForbiddenWithWwwAuthenticateHeader(scopes, ex);
//                return string.Empty;
//            }
//        }
//    }
//}
