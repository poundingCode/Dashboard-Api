﻿using Microsoft.Graph;
using RavenVision.Services.Contracts;
using System.Threading.Tasks;

namespace RavenVision.Services
{
    public class GraphService : IGraphService
    {
        private readonly GraphServiceClient _graphServiceClient;

        public GraphService(GraphServiceClient graphServiceClient)
        {
            _graphServiceClient = graphServiceClient;
        }
        public async Task<ISiteListsCollectionPage> GetSiteLists(string site)
        {
            var lists = await _graphServiceClient.Sites[site].Lists
            .Request()
            .GetAsync();
            return lists;
        }
    }
}
