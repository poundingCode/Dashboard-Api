﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RavenVision.Services
{
    public interface IGraphApiOperations
    {
        Task<dynamic> GetUserInformation(string accessToken);
        Task<string> GetPhotoAsBase64Async(string accessToken);

        Task<IDictionary<string, string>> EnumerateTenantsIdAndNameAccessibleByUser(ICollection<string> tenantIds, Func<string, Task<string>> getTokenForTenant);
    }
}
