﻿//using Microsoft.AspNetCore.Http;
//using Microsoft.Extensions.DependencyInjection;
//using Microsoft.Identity.Web;
//using RavenVision.Interfaces;
//using RavenVision.Models;
//using RavenVision.DataSources.API.Purchasing.ContractLevelCostBenchmark;

//namespace RavenVision.Services
//{
//    public static class ProxyService
//    {

//        public static void AddMyProxy(this IServiceCollection services)
//        {
//            var scopes = "User.Read";
//            var uri = "https://graph.microsoft.us/v1.0/";

//            services.AddHttpClient<IApiSource<CLCBM_Cagr_Data>, CompoundRateApiSource>(async config =>
//            {
//                string token = "";
//                var serviceProvider = services.BuildServiceProvider();
//                var httpContextAccessor = serviceProvider.GetRequiredService<IHttpContextAccessor>();
//                if (httpContextAccessor?.HttpContext?.User?.Identity?.IsAuthenticated ?? false)
//                {
//                    var tokenService = httpContextAccessor.HttpContext.RequestServices.GetRequiredService<ITokenAcquisition>();
//                    try
//                    {
//                        token = await tokenService.GetAccessTokenForUserAsync(new[] { scopes });
//                    }
//                    catch (System.Exception ex)
//                    {
//                        //logger.LogError(ex, "error while loading token for current user, try to login again");
//                    }
//                }

//                config.BaseAddress = new System.UriBuilder(uri).Uri;
//                if (!string.IsNullOrWhiteSpace(token))
//                {
//                    config.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
//                }
//            });
//        }
//    }
//}
